{include file="$view_dir/layouts/header.tpl"}
					<h3>Quên mật khẩu</h3>
						<i>Nhập vào email đã đăng ký, chúng tôi sẽ gửi tới bạn email xác minh reset mật khẩu</i>
						<hr>
						<form action="" method="POST" class="form-horizontal" role="form">
							{include file="$view_dir/layouts/frontend-response.tpl"}
							<div class="form-group">
								<label for="inputEmail" class="col-sm-2 control-label">Email:</label>
								<div class="col-sm-6">
									<input type="email" name="email" id="inputEmail" class="form-control" value="" required="required" title="">
								</div>
							</div>
						
							<div class="form-group">
								<div class="col-sm-10 col-sm-offset-2">
									<button type="submit" class="btn btn-primary">Gửi yêu cầu</button>
								</div>
							</div>
	
						</form>
						<div class="col-sm-6 col-xs-offset-2">
							<p><a href="login.html">Đăng nhập</a> / <a href="register.html">Đăng ký</a></p>
						</div>
{include file="$view_dir/layouts/footer.tpl"}