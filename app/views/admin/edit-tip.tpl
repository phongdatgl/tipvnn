{include file="$view_dir/layouts/header_admin.tpl"}
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	Hãy chú ý thời gian, thông tin Tip phải chính xác, chỉ up 1 lần và không chỉnh sửa. Nếu sai sẽ block tài khoản
</div>
<form action="" method="POST" class="form-horizontal" role="form">
		{include file="$view_dir/layouts/frontend-response.tpl"}
		<div class="form-group">
			<label for="inputStar" class="col-sm-2 control-label">Giờ:</label>
			<div class="col-sm-4">
				<div class='input-group datetimepicker'>
	            	<input type="text" name="start_time" id="inputTime_dep" readonly="true" data-date-format="YYYY-MM-DD HH:mm" class="form-control" value="{if $tip.start_time}{$tip.start_time}{else}{date('Y-m-d H:i')}{/if}" required="required" title="">
	            	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	            </div>

			</div>
		</div>
		<div class="form-group">
			<label for="league" class="col-sm-2 control-label">Mùa giải:</label>
			<div class="col-sm-6">
				<input type="text" name="league" id="league" class="form-control" value="{$tip.league}" title="">
			</div>
		</div>
		<div class="form-group">
			<label for="inputHome" class="col-sm-2 control-label">Chủ:</label>
			<div class="col-sm-6">
				<input type="text" name="home" id="inputHome" class="form-control" value="{$tip.home}" required="required" title="">
			</div>
		</div>
		<div class="form-group">
			<label for="inputOdds" class="col-sm-2 control-label">Odds:</label>
			<div class="col-sm-2">
				<input type="text" name="odds" id="inputOdds" class="form-control" value="{$tip.odds}" required="required"  title="">
			</div>
		</div>
		<div class="form-group">
			<label for="inputAway" class="col-sm-2 control-label">Khách:</label>
			<div class="col-sm-6">
				<input type="text" name="away" id="inputAway" class="form-control" value="{$tip.away}" required="required"  title="">
			</div>
		</div>
		<div class="form-group">
			<label for="inputTips" class="col-sm-2 control-label">Tips:</label>
			<div class="col-sm-6">
				<input type="text" name="tips" id="inputTips" class="form-control" value="{$tip.tips}" title="">
			</div>
		</div>
		<div class="form-group">
			<label for="inputScore" class="col-sm-2 control-label">Tỉ số:</label>
			<div class="col-sm-2">
				<input type="text" name="score" id="inputScore" class="form-control" value="{$tip.score}" placeholder="0-0" title="">
			</div>
		</div>
		<div class="form-group">
			<label for="inputType" class="col-sm-2 control-label">Loại:</label>
			<div class="col-sm-2">
				<select name="type" id="inputType" class="form-control" required="required">
					
					{$user = Sentry::getUser()}
					{if $user->hasAccess('uptipfree')}
					<option value="0" {if $tip.type==0}selected{/if}>Free</option>
					{/if}
					{if $user->hasAccess('testtip')}
					<option value="2" {if $tip.type==2}selected{/if}>Tip Test</option>
					{/if}
					{if $user->hasAccess('uptipvip')}
					<option value="3" {if $tip.type==3}selected{/if}>3 Sao</option>
					<option value="4" {if $tip.type==4}selected{/if}>4 Sao</option>
					<option value="5" {if $tip.type==5}selected{/if}>5 Sao</option>
					{/if}
				</select>
			</div>
		</div>
		<div id="isvip" class="{if $tip.type<3}vip-hidden{/if}">
		<div class="form-group">
			<label for="inputPrice" class="col-sm-2 control-label">Giá:</label>
			<div class="col-sm-2">
				<div class="input-group">
				  	<input type="text" name="price" id="inputPrice" class="form-control" value="{$tip.price}" pattern="\d+" title="">
				  	<span class="input-group-addon">$</span>
				</div>
				
			</div>
		</div>
		</div>
		<div class="form-group">
			<label for="inputResult" class="col-sm-2 control-label">Kết quả:</label>
			<div class="col-sm-2">
				<select name="result" id="input" class="form-control" required="required">
					<option value="0" {if $tip.result==0}selected{/if}>?</option>
					<option value="1" {if $tip.result==1}selected{/if}>WIN</option>
					<option value="2" {if $tip.result==2}selected{/if}>LOSE</option>
					<option value="3" {if $tip.result==3}selected{/if}>DRAW</option>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputInfo" class="col-sm-2 control-label">Info:</label>
			<div class="col-sm-10">
				<textarea rows="10" name="info" data-error-container="#editor_error" id="nicEditor" class="wysihtml5 form-control">{$tip.info}</textarea>
				<div id="editor1_error">
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</div>
</form>

{include file="$view_dir/layouts/footer.tpl"}