{include file="$view_dir/layouts/header_admin.tpl"}
{include file="$view_dir/layouts/frontend-response.tpl"}
<form action="" method="POST" class="form-horizontal" role="form">
	{foreach $sets as $k=>$v}
	<div class="form-group">
		<label for="input{$v.setting_key}" class="col-sm-2 control-label">{$v.setting_name}:</label>
		<div class="col-sm-10">
			<input type="text" name="{$v.setting_key}" id="input{$v.setting_key}" class="form-control" value="{$v.setting_value}" title="">
		</div>
	</div>
	{/foreach}
		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</div>
</form>
{include file="$view_dir/layouts/footer.tpl"}