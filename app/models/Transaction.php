<?php
class Transaction extends Model {
	protected static $table = 'transactions';
	protected static $hasMany = 'transaction_meta';
	protected static $key = 'trans_id';
	public static function checkTransactionCodeExist($code)
	{
		$result = DB::table(self::$table)->where('code', '=', $code)->first();
		if( count($result)) return true;
		return false;
	}
	public static function insertNewTransaction($data)
	{
		if(isset($data['meta'])) {
			$meta = $data['meta'];
			unset($data['meta']);
		}
		$trans_id = DB::table(self::$table)->insertGetId($data);
		self::insertToTransactionMeta($trans_id, $meta);
		return $trans_id;
	}
	protected static function insertToTransactionMeta($trans_id, $meta)
	{
		foreach ($meta as $key => $value) {
			$arr = array('trans_id'=>$trans_id, 'meta_key'=>$key, 'meta_value'=>$value);
			DB::table(self::$hasMany)->insert($arr);
		}
		
	}
	public static function getTransactionList($userId = false, $limit = false, $offset = false, $keyword = '', $status = 'all')
	{
		$result = DB::table(self::$table);
		$result->select(array(self::$table . '.id as id', 'code', 'create_at', 'total', 'type', 'status', 'user_id', 'buy_tip_id', 'username', 'users.id as uid'));
		if($limit !== FALSE) {
			$result->limit($limit)->offset($offset);
		}
		if($userId !== FALSE) {
			$result->where('user_id', '=', $userId);
		}
		if(is_numeric($status)) $result->where('status', '=', $status);
		if($keyword != '') {
			$result->where('code', '=', $keyword);
			$result->orWhere('username', 'like', '%'.$keyword.'%');
		}
		$result->join('users', 'users.id', '=', self::$table . '.user_id', 'left');
		
		return $result->orderBy(self::$table.'.id', 'desc')->get();
	}
	public static function getTransactionById($userId = false, $id = false)
	{
		$result = DB::table(self::$hasMany)
				->join(self::$table, self::$table . '.id', '=', self::$hasMany . '.'.self::$key)
				->where('user_id', '=', $userId)
				->where('trans_id', '=', $id)
				->get();
		$data = array();
		if(count($result) == 0) return $data;
		foreach ($result as $key => $value) {
			$data['name'][self::transactionMeta($value['meta_key'])] = $value['meta_value'];
		}
		$data['code'] = $result[0]['code'];		
		$data['total'] = $result[0]['total'];		
		$data['status'] = $result[0]['status'];		
		$data['trans_id'] = $result[0]['trans_id'];	
		return $data;
	}
	protected static function transactionMeta($meta_key)
	{
		$key = array(
			'account_number'	=>	'Số tài khoản',
			'account_name'	=>	'Tên tài khoản',
			'deposit_time'	=>	'Thời gian',
			'bank_name'	=>	'Tên ngân hàng',
			'meta_type'	=>	'Loại',
			'tip_date'	=>	'Giờ bắt đầu',
			'tip_league'	=>	'Mùa giải',
			'tip_vs'	=>	'Trận',
			'tip_odds'	=>	'ODDS',
			'tip_tips'	=>	'Tips',
			'tip_price'	=>	'Giá',
			'aff_type'	=>	'Loại giao dịch',
			'aff_total'	=>	'Tổng tiền bán tip',
			'aff_tran'	=>	'Trận',
			'aff_percent'=>	'Hoa hồng',
			'aff_time'	=>	'Thời gian',
			'ref_type'	=>	'Loại',
			'ref_bought'=>	'Tiền đã mua',
			'ref_receive'=>	'Nhận lại',
			'ref_percent'=>	'Hoàn trả',
			'ref_time'=>	'Thời gian',

			);
		foreach ($key as $k => $v) {
			if($meta_key == $k) return $v;
		}
		return '';
	}
	public static function getListTipBought()
	{
		$user = Sentry::getUser();
		if( ! Sentry::check()) {
			$userid = 0;
		} else {
			$userid = $user->id;
		}
		$result = DB::table(self::$table)
		->where('user_id', '=', $userid)
		->where('buy_tip_id', '<>', 0)
		->lists('buy_tip_id');
		return $result;
	}
	public static function getTipBoughtByTipId($tipId)
	{
		$result = DB::table(self::$table)
		->select(DB::raw('sum(total) as totalmoney'))
		->where('buy_tip_id', '=', $tipId)
		->first();
		return $result['totalmoney'];

	}
	public static function getUsersBought($tipId)
	{
		$result = DB::table(self::$table)
		->where('buy_tip_id', '=', $tipId)
		->get(array('user_id', 'total'));
		return $result;
	}
	public static function adminViewTrans($id)
	{
		$result = DB::table(self::$table)
		->select(array(self::$table.'.id as id', 'code','create_at', 'total','user_id', 'buy_tip_id','type','status','username'))
		->join('users', 'users.id', '=', 'user_id', 'left')
		->where(self::$table.'.id', '=', $id)->first();
		if(count($result)) $result['meta'] = self::transMeta($id);
		return $result;
	}
	protected static function transMeta($id)
	{
		$result = DB::table('transaction_meta')->where('trans_id','=',$id)->get();
		$newResult = array();
		foreach ($result as $key => $value) {
			$newResult[] = array('name'=>self::transactionMeta($value['meta_key']), 'meta_value'=>$value['meta_value']);
			
		}
		return $newResult;
	}
	public static function saveStatus($id, $status)
	{
		return DB::table(self::$table)->where('id', '=', $id)->update(array('status'=>$status));
	}

}