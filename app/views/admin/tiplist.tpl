{include file="$view_dir/layouts/header_admin.tpl"}
{include file="$view_dir/layouts/frontend-response.tpl"}
<a href="{$admin_dir}/add-tip.html" class="btn pull-right btn-success">Thêm TIP</a>
<table class="table table-hover table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Giờ</th>
			<th>Chủ nhà</th>
			<th>ODDS</th>
			<th>Khách</th>
			<th>Tips</th>
			<th>Kết Quả</th>
			<th>Tỉ số</th>
			<th>Loại</th>
			
			<th>Giá</th>
			{$user = Sentry::getUser()}
			{if $user->hasAccess('admin')}
			<th>Giá</th>
			{/if}
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		{foreach $tiplist as $v}
		<tr>
			<td>{$stt}</td>
			<td>{$v.start_time}</td>
			<td>{$v.home}</td>
			<td>{$v.odds}</td>
			<td>{$v.away}</td>
			<td>{$v.tips}</td>
			<td>{if $v.result==0}
				<span class="label label-default"><i class="fa fa-question"></i></span>
				{else if $v.result==1}
				<span class="label label-success tip-win">WIN</span>
				{else if $v.result==2}
				<span class="label label-danger">LOSE</span>
				{else}<span class="label label-info">DRAW</span>{/if}
			</td>
			<td>{$v.score}</td>
			<td class="tip-type t{$v.type}-star">{if $v.isvip==1}{for $i=1 to $v.type}<i class="fa fa-star"></i>{/for}{else}{if $v.type==2}<span class="label label-danger">TEST TIP</span>{else}<span class="label label-warning">FREE</span>{/if}{/if}</td>
			
			<td>{moneyFormat($v.price)} $</td>
			{if $user->hasAccess('admin')}
			<td><a href="{$admin_dir}/edit-user-{$v.uid}.html">{$v.username}</a></td>
			{/if}
			<td>{if $user->hasAccess('uptipvip')}<a href="{$admin_dir}/edit-tip-{$v.id}.html" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a> <a href="{$admin_dir}/delete-tip-{$v.id}.html" onclick="return confirm('Bạn có muốn xoá không ?');" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-close"></i></a> {/if}</td>
		</tr>
		{$stt = $stt + 1}
		{/foreach}
	</tbody>
</table>
<div id="pagination" class="pull-center">
	{$pagination}
</div>

{include file="$view_dir/layouts/footer.tpl"}