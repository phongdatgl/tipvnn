{include file="$view_dir/layouts/header_admin.tpl"}

<form action="" method="POST" class="form-horizontal" role="form">
		{include file="$view_dir/layouts/frontend-response.tpl"}
		
		<div class="form-group">
			<label for="page_name" class="col-sm-2 control-label">Title:</label>
			<div class="col-sm-10">
				<input type="text" name="page_name" id="page_name" class="form-control" value="{$page.page_name}" title="">
			</div>
		</div>
		<div class="form-group">
			<label for="inputInfo" class="col-sm-2 control-label">Info:</label>
			<div class="col-sm-10">
				<textarea rows="10" name="page_content" data-error-container="#editor_error" class="wysihtml5 form-control">{$page.page_content}</textarea>
				<div id="editor1_error">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="inputOrder" class="col-sm-2 control-label">Sắp xếp:</label>
			<div class="col-sm-1">
				<input type="text" name="sortorder" id="inputOrder" class="form-control" value="{$page.sortorder}" pattern="\d+" title="">
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</div>
</form>

{include file="$view_dir/layouts/footer.tpl"}