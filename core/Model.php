<?php 
class Model {
	public static function all($limit = false, $offset = false)
	{
		$ins = new static;
		$result = DB::table($ins->table);
		if($limit) $result->limit($limit)->offset($offset);
		$result->orderBy('id', 'desc'); 
		return $result->get();
	}
	public static function find($id, $column = array('*'))
	{
		$ins = new static;
		if(isset($ins->findable))
			$result = DB::table($ins->table)->where($ins->findable, '=', $id)->first($column);
		else 
			$result = DB::table($ins->table)->where('id', '=', $id)->first($column);
		return $result;
	}
	public static function findOrFail($id, $column = array('*'))
	{
		$result = self::find($id, $column);
		if( ! count($result))
		{
			return "404";
		}
		return $result;
	}
}

