{include file="$view_dir/layouts/header_admin.tpl"}
<h3>Chi tiết giao dịch #{$trans.code}</h3>
<div class="col-xs-6">
<a href="{$admin_dir}/transactions.html" class="btn btn-success">Quay lại</a>
<input type="hidden" id="trans-id" value="{$trans.id}">
<table class="table table-hover">
	<tbody>
		<tr>
			<td>Sửa</td>
			<td>
				<div class="col-sm-6">
					<select name="" id="new-status" class="form-control" required="required">
						<option value="1" {if $trans.status==1}selected{/if}>Waiting</option>
						<option value="2" {if $trans.status==2}selected{/if}>Completed</option>
						<option value="3" {if $trans.status==3}selected{/if}>Cancelled</option>
					</select>
				</div>
				<div class="col-sm-3"><button type="button" id="save-transaction" class="btn btn-info">SAVE</button></div>
				<div class="col-sm-2" id="loading-result"></div>
			</td>
		</tr>
		<tr>
			<td>Mã giao dịch</td>
			<td>{$trans.code}</td>
		</tr>
		<tr>
			<td>Ngày tạo</td>
			<td>{$trans.create_at}</td>
		</tr>
		<tr>
			<td>Tổng tiền</td>
			<td>{$trans.total}</td>
		</tr>
		<tr>
			<td>User</td>
			<td>{$trans.username}</td>
		</tr>
		<tr>
			<td>Trạng thái</td>
			<td id="trans-result">{if $trans.status==1}
				<span class="label label-warning">Waiting</span>
				{else if $trans.status==2}
				<span class="label label-success">Completed</span>
				{else}
				<span class="label label-danger">Cancelled</span>
				{/if}</td>
		</tr>
		{foreach $trans.meta as $v}
		<tr>
			<td>{$v.name}</td>
			<td>{$v.meta_value}</td>
		</tr>
		{/foreach}

	</tbody>
</table>
</div>
{include file="$view_dir/layouts/footer.tpl"}