<?php 
//Dùng Silex Application. Xem document tại: http://silex.sensiolabs.org/documentation
//$request->getUriForPath
use Symfony\Component\HttpFoundation\Response;
//Home page
$checkLogged = function(Request $request) use ($app) {
	if( Sentry::check()) {
		return $app->redirect('/index.html');
	}
};
$checkNotLog = function(Request $request) use ($app) {
	$referer = $request->server->get('REQUEST_URI');
	if( ! Sentry::check()) {
		return $app->redirect('/login.html?redirect_to=' . urlencode($referer));
	}
};
$checkAdmin = function(Request $request) use ($app) {
	$referer = $request->server->get('REQUEST_URI');
	if( ! Sentry::check()) {
		return $app->redirect('/login.html?redirect_to=' . urlencode($referer));
	}
	$user = Sentry::getUser();
	if( ! $user->hasAccess('admin')) {
		return new Response('Trang này không tìm thấy', 404 /* ignored */, array('X-Status-Code' => 200, 'Content-Type'=>'text/html'));
	}
};
$checkUpTip = function(Request $request) use ($app) {
	$referer = $request->server->get('REQUEST_URI');
	if( ! Sentry::check()) {
		return $app->redirect('/login.html?redirect_to=' . urlencode($referer));
	}
	$user = Sentry::getUser();
	if( ! $user->hasAccess('uptipfree') && ! $user->hasAccess('testtip')) {
		return new Response('Trang này không tìm thấy', 404 /* ignored */, array('X-Status-Code' => 200, 'Content-Type'=>'text/html'));
	}
};
/*
 $group = Sentry::createGroup(array(
        'name'        => 'Test Tipser',
        'permissions' => array(
            ),
    ));
*/
$app->get('/index.html', 'HomeController::index');
$app->get('/', 'HomeController::index');
//login
$app->get('/login.html', 'AuthenController::login')->before($checkLogged);
$app->post('/login.html', 'AuthenController::doLogin')->before($checkLogged);

//register
$app->get('/register.html', 'AuthenController::register')->before($checkLogged);
$app->post('/register.html', 'AuthenController::doRegister')->before($checkLogged);
$app->get('/captcha.html', 'AuthenController::createCaptcha');

//fogot
$app->get('/forgot.html', 'AuthenController::forgot')->before($checkLogged);
$app->post('/forgot.html', 'AuthenController::doForgot')->before($checkLogged);
//reset
$app->get('/confirm-forgot/{code}.html', 'AuthenController::reset')->before($checkLogged);
$app->post('/confirm-forgot/{code}.html', 'AuthenController::doReset')->before($checkLogged);

//profile
$app->get('/profile.html', 'AuthenController::profile')->before($checkNotLog);
$app->post('/profile.html', 'AuthenController::doUpdateProfile')->before($checkNotLog);
//profile
$app->get('/change-pass.html', 'AuthenController::changepass')->before($checkNotLog);
$app->post('/change-pass.html', 'AuthenController::doChangepass')->before($checkNotLog);

$app->get('/logout.html', function() use ($app) {
	Sentry::logout();
	return $app->redirect('/index.html', 301);
});


//tips
$app->get('/tips.html', 'HomeController::tips');//->before($checkNotLog);
$app->get('/tips-{page}.html', 'HomeController::tips')->assert('page', '\d+');//->before($checkNotLog)
//free tips
$app->get('/free-tips.html', 'HomeController::freetips');
$app->get('/free-tips-{page}.html', 'HomeController::freetips')->assert('page', '\d+');
//test tips
$app->get('/test-tips.html', 'HomeController::testtips');
$app->get('/test-tips-{page}.html', 'HomeController::testtips')->assert('page', '\d+');

//lien hệ
$app->get('/contact.html', 'HomeController::contact');
$app->post('/contact.html', 'HomeController::doContact');
//faqs
$app->get('/faqs.html', 'HomeController::faqs');

//tipser info
$app->get('/tipser-info-{userid}.html', 'HomeController::tipserInfo')->assert('userid', '\d+');
$app->post('/ajax.html', 'HomeController::ajax');

$app->get('/deposit.html', 'BankingController::bankDeposit')->before($checkNotLog);
$app->post('/deposit.html', 'BankingController::doBankDeposit')->before($checkNotLog);
$app->get('/baokim.html', 'BankingController::baokimDeposit')->before($checkNotLog);
$app->get('/history.html', 'HomeController::transactionHistory')->before($checkNotLog);
$app->get('/history-{page}.html', 'HomeController::transactionHistory')->before($checkNotLog)->assert('page', '\d+');
$app->get('/view-{id}.html', 'HomeController::viewTransaction')->before($checkNotLog)->assert('id', '\d+');
$app->get('/buy-tip-{id}.html', 'HomeController::buyTip')->before($checkNotLog)->assert('id', '\d+');
$app->get('/confirm-{id}.html', 'HomeController::buyConfirm')->before($checkNotLog)->assert('id', '\d+');
$app->get('/viewtip-{id}.html', 'HomeController::viewTip')->before($checkNotLog)->assert('id', '\d+');

$admin_dir = '/admin';
define('ADMIN_DIR', $admin_dir);
$app['smarty']->assign('admin_dir', $admin_dir);
//Admin
$app->get( $admin_dir . '/', 'AdminController::index')->before($checkUpTip);
$app->get( $admin_dir . '/index.html', 'AdminController::index')->before($checkUpTip);
$app->get( $admin_dir . '/tiplist.html', 'AdminController::tiplist')->before($checkUpTip);
$app->get( $admin_dir . '/tiplist-{page}.html', 'AdminController::tiplist')->before($checkUpTip)->assert('id', '\d+');
$app->get( $admin_dir . '/edit-tip-{id}.html', 'AdminController::tipEdit')->before($checkUpTip)->assert('id', '\d+');
$app->post( $admin_dir . '/edit-tip-{id}.html', 'AdminController::doTipEdit')->before($checkUpTip)->assert('id', '\d+');
$app->get( $admin_dir . '/add-tip.html', 'AdminController::tipAdd')->before($checkUpTip);
$app->post( $admin_dir . '/add-tip.html', 'AdminController::doTipAdd')->before($checkUpTip);
$app->get( $admin_dir . '/delete-tip-{id}.html', function($id) use($app) {
	$user = Sentry::getUser();
	if( $user->hasAccess('uptipvip')) {
		Tiplist::deleteTip($id);
		$app['session']->getFlashBag()->add('success', 'Xoá thành công !');
	}
	
	return $app->redirect(ADMIN_DIR . '/tiplist.html', 301);
})->before($checkUpTip)->assert('id', '\d+');

//user
$app->get( $admin_dir . '/users.html', 'AdminController::userlist')->before($checkAdmin);
$app->get( $admin_dir . '/users-{page}.html', 'AdminController::userlist')->before($checkAdmin)->assert('id', '\d+');
$app->get( $admin_dir . '/add-user.html', 'AdminController::addUser')->before($checkAdmin);
$app->post( $admin_dir . '/add-user.html', 'AdminController::doAddUser')->before($checkAdmin);
$app->get( $admin_dir . '/edit-user-{id}.html', 'AdminController::editUser')->before($checkAdmin)->assert('id', '\d+');
$app->post( $admin_dir . '/edit-user-{id}.html', 'AdminController::doEditUser')->before($checkAdmin)->assert('id', '\d+');
$app->get( $admin_dir . '/delete-user-{id}.html', 'AdminController::deleteUser')->before($checkAdmin)->assert('id', '\d+');
$app->get( $admin_dir . '/settings.html', 'AdminController::settings')->before($checkAdmin);
$app->post( $admin_dir . '/settings.html', 'AdminController::doSaveSettings')->before($checkAdmin);

$app->get( $admin_dir . '/refund-{id}.html', 'AdminController::doTipMoneyAction')->before($checkAdmin);
$app->get( $admin_dir . '/transactions.html', 'AdminController::transactions')->before($checkAdmin);
$app->get( $admin_dir . '/transactions-{page}.html', 'AdminController::transactions')->before($checkAdmin)->assert('id', '\d+');
$app->get( $admin_dir . '/view-trans-{id}.html', 'AdminController::viewTransaction')->before($checkAdmin)->assert('id', '\d+');
$app->get( $admin_dir . '/pages.html', 'AdminController::viewPage')->before($checkAdmin);
$app->get( $admin_dir . '/edit-page-{id}.html', 'AdminController::editPage')->before($checkAdmin)->assert('id', '\d+');
$app->post( $admin_dir . '/edit-page-{id}.html', 'AdminController::doEditPage')->before($checkAdmin)->assert('id', '\d+');
$app->get( $admin_dir . '/add-page.html', 'AdminController::addPage')->before($checkAdmin);
$app->post( $admin_dir . '/add-page.html', 'AdminController::doAddPage')->before($checkAdmin);
$app->post( $admin_dir . '/save-transaction.html', 'AdminController::saveTransaction')->before($checkAdmin);