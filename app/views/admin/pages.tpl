{include file="$view_dir/layouts/header_admin.tpl"}
{include file="$view_dir/layouts/frontend-response.tpl"}
<a href="{$admin_dir}/add-page.html" class="btn btn-success">Add Page</a>
<table class="table table-hover table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Title</th>
			<th>Order</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		{foreach $pages as $v}
		<tr>
			<td>{$stt}</td>
			<td>{$v.page_name}</td>
			<td>{$v.sortorder}</td>
			
			<td><a href="{$admin_dir}/edit-page-{$v.id}.html" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a> <a href="{$admin_dir}/delete-page-{$v.id}.html" onclick="return confirm('Bạn có muốn xoá không ?');" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-close"></i></a> </td>
		</tr>
		{$stt = $stt + 1}
		{/foreach}
	</tbody>
</table>
{include file="$view_dir/layouts/footer.tpl"}