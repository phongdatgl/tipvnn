{include file="$view_dir/layouts/header.tpl"}
						<h3>Lịch sử giao dịch</h3>
						<hr>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Mã giao dịch</th>
									<th>Ngày</th>
									<th>Số tiền</th>
									<th>Trạng thái</th>
									<th>Xem</th>
								</tr>
							</thead>
							<tbody>
								{foreach $transactions as $v}
								<tr>
									<td>{$stt}</td>
									<td>{$v.code}</td>
									<td>{$v.create_at}</td>
									<td>
										{if $v.type==1}<span class="money-up">+</span>{else}<span class="money-down">-</span>{/if} {moneyFormat($v.total)}
									</td>
									<td>
										{if $v.status==1}
										<span class="label label-warning">Đang chờ</span>
										{else if $v.status==2}
										<span class="label label-success">Hoàn thành</span>
										{else}
										<span class="label label-default">Đã huỷ</span>
										{/if}
									</td>
									<td><a href="{$asset}view-{$v.id}.html" class="btn btn-sm btn-primary">Xem</a></td>
								</tr>
								{$stt = $stt + 1}
								{/foreach}
							</tbody>
						</table>
						<div id="pagination" class="pull-center">
							{$pagination}
						</div>
{include file="$view_dir/layouts/footer.tpl"}