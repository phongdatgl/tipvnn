{include file="$view_dir/layouts/header_admin.tpl"}
{include file="$view_dir/layouts/frontend-response.tpl"}

<div role="tabpanel">
<ul class="nav nav-tabs" role="tablist">
	<li role="presentation" class=""><a href="{$admin_dir}/transactions.html">Tất cả</a></li>
	<li role="presentation"><a href="{$admin_dir}/transactions.html?status=1">Đang chờ</a></li>
	<li role="presentation"><a href="{$admin_dir}/transactions.html?status=2">Hoàn Thành</a></li>
	<li role="presentation"><a href="{$admin_dir}/transactions.html?status=3">Đã Huỷ</a></li>
</ul>
</div>

<form action="" method="GET" class="form-inline pull-right" role="form">
	<div class="form-group">
		<div class="col-sm-10">
			<input type="text" name="keyword" id="inputKeyword" placeholder="Code, Username" class="form-control" value="{if isset($keyword)}{$keyword}{/if}" title="">
		</div>
	</div>
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
<table class="table table-hover table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Mã giao dịch</th>
			<th>Ngày tạo</th>
			<th>Total</th>
			<th>Người dùng</th>
			<th>Trạng thái</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		{foreach $transactions as $v}
		<tr>
			<td>{$stt}</td>
			<td>{$v.code}</td>
			<td>{$v.create_at}</td>
			<td>{$v.total} $</td>
			<td><a href="{$admin_dir}/edit-user-{$v.uid}.html">{$v.username}</a></td>
			<td>{if $v.status==1}
				<span class="label label-warning">Waiting</span>
				{else if $v.status==2}
				<span class="label label-success">Completed</span>
				{else}
				<span class="label label-danger">Cancelled</span>
				{/if}
			</td>
			
			<td><a href="{$admin_dir}/view-trans-{$v.id}.html" title="View" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a> <a href="{$admin_dir}/delete-trans-{$v.id}.html" onclick="return confirm('Bạn có muốn xoá không ?');" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-close"></i></a> </td>
		</tr>
		{$stt = $stt + 1}
		{/foreach}
	</tbody>
</table>
<div id="pagination" class="pull-center">
	{$pagination}
</div>

{include file="$view_dir/layouts/footer.tpl"}