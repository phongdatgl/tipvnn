<?php 
class Pages extends Model {
	protected static $table = 'pages';
	public static function getPage($type = 1)
	{
		$pages = DB::table('pages')->where('page_type', '=', $type)->orderBy('sortorder', 'asc')->get();
		return $pages;
	}
	public static function getPageById($id)
	{
		return DB::table(self::$table)->where('id', '=', $id)->first();
		
	}
	public static function updatePage($data, $id) 
	{
		$result = DB::table(self::$table)->where('id','=', $id)->update($data);
		return $result;
	}
	public static function insertNewPage($data) 
	{
		return DB::table(self::$table)->insert($data);
	}
}
?>