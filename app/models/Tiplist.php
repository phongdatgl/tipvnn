<?php 
class Tiplist extends Model {
	protected static $table = 'tiplist';
	public static function findTipVip($start = false, $limit = false, $id = FALSE)
	{
		$result = DB::table(self::$table)->where('isvip', '=', 1);
		if($id !== FALSE) $result->where('id', '=', $id);
		if($start !== FALSE) $result->offset($start);
		if($limit !== FALSE) $result->limit($limit);
		return $result->orderBy('start_time', 'desc')->get();
	}
	public static function totalTipVip()
	{
		return count(self::findTipVip());
	}
	public static function findTipFree($start = false, $limit = false, $id = FALSE, $where = array())
	{
		
		$result = DB::table(self::$table)
				->select(array('tiplist.id','start_time', 'league', 'home', 'odds', 'away', 'tips', 'score', 'result', 'username', 'user_id'))
				->join('users', 'users.id', '=', 'user_id')
				->where('isvip', '=', 0);
		foreach ($where as $key => $value) {
			$result->where($key, '=', $value);
		}
		if($id !== FALSE) $result->where('id', '=', $id);
		if($start !== FALSE) $result->offset($start);
		if($limit !== FALSE) $result->limit($limit);
		$result = $result->orderBy('start_time', 'desc')->get();
		$users = array();
		$newResult = array();
		foreach ($result as $key => $value) {
			$tmp = $value;
			if ( array_key_exists($value['user_id'], $users)) {
				$tmp['percent'] = $users[$value['user_id']];
			} else {
				$r = DB::table(self::$table)
				->where('user_id', '=', $value['user_id'])
				->where('isvip','=',0)
				->where('start_time', '>', strtotime(time()+7200))
				->limit(30)
				->get();
				
				$result = DB::select(
					DB::raw('select user_id uid, username, (select COUNT(*) 
						from `tiplist` where user_id=uid and `result` = 1 and `isvip` = 0 order by id desc limit 0, 30) win,
						(select COUNT(*) from `tiplist` where user_id=uid and `result` = 2 and `isvip` = 0 order by id desc limit 0, 30) lose
					from `tiplist` inner join `users` on `users`.`id` = `tiplist`.`user_id` 
					where user_id = '.$value["user_id"].'
					group by `uid` order by win desc'));	
				$result = $result[0];
				if($result['lose'] != 0 || $result['win'] != 0) {
					$percent = ($result['win'] / ($result['win'] + $result['lose']))*100;
				} else {
					$percent = 0;
				}

				$tmp['percent'] = $percent;
				$users[$value['user_id']] = $percent;
			}
			$newResult[] = $tmp;
		}
		return $newResult;
	}
	public static function findTipTest($start = false, $limit = false, $id = FALSE, $where = array())
	{
		
		$result = DB::table(self::$table)
				->select(array('tiplist.id','start_time', 'league', 'home', 'odds', 'away', 'tips', 'score', 'result', 'username', 'user_id'))
				->join('users', 'users.id', '=', 'user_id')
				->where('isvip', '=', 2);
		foreach ($where as $key => $value) {
			$result->where($key, '=', $value);
		}
		if($id !== FALSE) $result->where('id', '=', $id);
		if($start !== FALSE) $result->offset($start);
		if($limit !== FALSE) $result->limit($limit);
		$result = $result->orderBy('start_time', 'desc')->get();
		$users = array();
		$newResult = array();
		foreach ($result as $key => $value) {
			$tmp = $value;
			if ( array_key_exists($value['user_id'], $users)) {
				$tmp['percent'] = $users[$value['user_id']];
			} else {
				$r = DB::table(self::$table)
				->where('user_id', '=', $value['user_id'])
				->where('isvip','=',2)
				->where('start_time', '>', strtotime(time()+7200))
				->limit(30)
				->get();
				
				$result = DB::select(
					DB::raw('select user_id uid, username, (select COUNT(*) 
						from `tiplist` where user_id=uid and `result` = 1 and `isvip` = 2 order by id desc limit 0, 30) win,
						(select COUNT(*) from `tiplist` where user_id=uid and `result` = 2 and `isvip` = 2 order by id desc limit 0, 30) lose
					from `tiplist` inner join `users` on `users`.`id` = `tiplist`.`user_id` 
					where user_id = '.$value["user_id"].'
					group by `uid` order by win desc'));	
				$result = $result[0];
				if($result['lose'] != 0 || $result['win'] != 0) {
					$percent = ($result['win'] / ($result['win'] + $result['lose']))*100;
				} else {
					$percent = 0;
				}

				$tmp['percent'] = $percent;
				$users[$value['user_id']] = $percent;
			}
			$newResult[] = $tmp;
		}
		return $newResult;
	}
	public static function checkTipWin($result)
	{
		$error = 'error';
		
		$arr1 = array('Over', 'Under', 'over', 'under', ' ');
		if(stristr($result['tips'], 'Over') || stristr($result['tips'], 'over')) $type = 1;
		else $type = 0;
		$tips = str_replace($arr1, '', $result['tips']);
		if( ! is_numeric($tips)) return $error;
		preg_match_all('/\d+/', $result['score'], $score);
		if( ! isset($score[0][0]) || ! isset($score[0][1])) return $error;
		$totalScore = $score[0][0] + $score[0][1];
		if( ! is_numeric($totalScore)) return $error;
		if($type == 1) {//cuoc tren ti so la win
			if($totalScore > $tips) return true;
		} else {
			if($totalScore < $tips) return true;
		}
		return false;
	}
	public static function refundMoney()
	{

	}
	public static function addTipserBalance()
	{

	}
	public static function getUsersBoughtTip($tipId)
	{
		$result = DB::table(self::$table)->where('id', '=', $tipId)->get();

	}

	public static function totalTipFree()
	{
		return count(self::findTipFree());
	}
	public static function getTipById($id)
	{
		return DB::table(self::$table)->where('id', '=', $id)->where('isvip', '=', 1)->first();
	}
	public static function getAllTips($start = false, $limit = false)
	{
		$user = Sentry::getUser();
		$result = DB::table(self::$table);
		$result->select(array('tiplist.id as id', 'start_time', 'home', 'away', 'odds', 'tips', 'result', 'score', 'type', 'price', 'username', 'isvip', 'users.id as uid'));
		$result->join('users', 'users.id', '=', self::$table . '.user_id', 'left');
		if( ! $user->hasAccess('admin')) {
			if( ! $user->hasAccess('edittip')) {
				$result->where('user_id', '=', $user->id);
			}
		}
		if($start !== FALSE) $result->offset($start);
		if($limit !== FALSE) $result->limit($limit);
		return $result->orderBy('start_time', 'desc')->get();
	}
	public static function getTipInfo($id)
	{
		$user = Sentry::getUser();
		$result = DB::table(self::$table);
		if( ! $user->hasAccess('admin') && ! $user->hasAccess('edittip')) $result->where('user_id', '=', $user->id);
		$result = $result->where('id', '=', $id)->first();
		return $result;
	}
	public static function updateTip($data, $id)
	{
		return DB::table(self::$table)->where('id', '=', $id)->update($data);
	}
	public static function insertTip($data)
	{
		$user = Sentry::getUser();
		$data['user_id'] = $user->id;
		return DB::table(self::$table)->insert($data);
	}
	public static function deleteTip($id)
	{
		return DB::table(self::$table)->where('id', '=', $id)->delete();
	}
	public static function topTipser($isYear = false)
	{
		if($isYear) {
			$result = DB::select(DB::raw('select user_id uid, username, (select COUNT(*) from `tiplist` where user_id=uid and `result` = 1 and `isvip` = 1 and YEAR(`start_time`) = '.date("Y").' AND MONTH(`start_time`)='.date("m").') win,(select COUNT(*) from `tiplist` where user_id=uid and `result` = 2 and `isvip` = 1 and YEAR(`start_time`) = '.date("Y").' AND MONTH(`start_time`)='.date("m").') lose,(select COUNT(*) from `tiplist` where user_id=uid and `result` = 3 and `isvip` = 1 and YEAR(`start_time`) = '.date("Y").' AND MONTH(`start_time`)='.date("m").') draw from `tiplist` inner join `users` on `users`.`id` = `tiplist`.`user_id` group by `uid` order by win desc limit 0, 5'));
		} else {
			$result = DB::select(DB::raw('select user_id uid, username, (select COUNT(*) from `tiplist` where user_id=uid and `result` = 1 and `isvip` = 1) win,(select COUNT(*) from `tiplist` where user_id=uid and `result` = 2 and `isvip` = 1) lose,(select COUNT(*) from `tiplist` where user_id=uid and `result` = 3 and `isvip` = 1) draw from `tiplist` inner join `users` on `users`.`id` = `tiplist`.`user_id` group by `uid` order by win desc limit 0, 5'));	
		}
		
		return $result;
	}
	public static function tipByUser($userid)
	{
		
		$data = array();
		$result = DB::table(self::$table)
			->select(DB::raw('user_id, users.id as uid, username, COUNT(*) as totaltip, 
				(SELECT COUNT(*) FROM tiplist WHERE isvip=0 AND user_id=uid and result=1) as win,
				(SELECT COUNT(*) FROM tiplist WHERE isvip=0 AND user_id=uid and result=2) as lose,
				(SELECT COUNT(*) FROM tiplist WHERE isvip=0 AND user_id=uid and result=3) as draw
				'))
			->join('users', 'users.id','=','user_id')
			->where('isvip', '=', '0')
			->groupBy('user_id')
			->orderBy('totaltip', 'desc')
			->get(); 
		$position = $total = 0;
		foreach ($result as $key => $value) {
			$position += 1;
			$data['position'] = $position;
			$data['username'] = $value['username'];
			$data['win'] = $value['win'];
			$data['lose'] = $value['lose'];
			$data['draw'] = $value['draw'];
			$data['totaltip'] = $value['totaltip'];
			$data['total'] = count($result);
			if($userid == $value['user_id']) {
				break;
			}
		}
		return $data;
	}
	public static function checkAddTip($user, $date)
	{
		$tip_per_day = 2;
		$date = $date . ':00';
		$date = strtotime($date);
		if( ! $user->hasAccess('uptipvip')) {
			$result = DB::table(self::$table)->select(DB::raw('YEAR(start_time) as y, MONTH(start_time) m, DAY(start_time) d'))
				->where('user_id', '=', $user->id)
				->whereRaw('YEAR(start_time) = ' . date("Y", $date))
				->whereRaw('MONTH(start_time) = ' . date("m", $date))
				->whereRaw('DAY(start_time) = ' . date("d", $date))
				->get();
			if(count($result) > 2) {
				return false;
			}
		}
		return true;
	}
	
}
?>