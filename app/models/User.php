<?php 
class User extends Model {
	protected static $table = 'users';
	protected $findable = 'id';

	public static function checkUserExist($username, $email)
	{
		$ck = DB::table('users')->where('username', '=', $username)->orWhere('email','=',$email)->first();
		if(count($ck)) return false;
		else return true;
	}
	public static function getUsers($limit = false, $offset = false, $keyword = '') 
	{
		$result = DB::table(self::$table);
		$result->select(array('*', self::$table.'.id as uid', 'groups.name as gname', self::$table.'.created_at as ucreated_at'));
		if($keyword != NULL) {
			$result->where('username', 'like', '%'.$keyword.'%')
					->orWhere('email', 'like', '%'.$keyword.'%');
		}
		if($limit) $result->limit($limit)->offset($offset);
		$result->join('users_groups', 'users_groups.user_id', '=', self::$table . '.id');
		$result->join('groups', 'groups.id','=', 'users_groups.group_id');
		$result->orderBy('users.id', 'desc'); 
		return $result->get();
	}

	public static function refundTip($data) 
	{
		foreach ($data as $value) {
			DB::table(self::$table)->where('id', '=', $value['user'])->update(array('balance'=>DB::raw('balance + ' . $value['total'])));
		}
		return true;
	}
}