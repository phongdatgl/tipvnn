					{include file="$view_dir/layouts/header.tpl"}
					<h3>Liên Hệ</h3>
						<hr>
						<form action="" method="POST" class="form-horizontal" role="form">
							{include file="$view_dir/layouts/frontend-response.tpl"}
							<div class="form-group">
								<label for="inputName" class="col-sm-2 control-label">Tên bạn:</label>
								<div class="col-sm-6">
									<input type="text" name="name" id="inputName" class="form-control" value="" required="required" title="">
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail" class="col-sm-2 control-label">Email:</label>
								<div class="col-sm-6">
									<input type="email" name="email" id="inputEmail" class="form-control" value="" required="required" title="">
								</div>
							</div>
							<div class="form-group">
								<label for="inputSubject" class="col-sm-2 control-label">Tiêu đề:</label>
								<div class="col-sm-6">
									<input type="text" name="subject" id="inputSubject" class="form-control" value="" required="required" title="">
								</div>
							</div>
							<div class="form-group">
								<label for="input" class="col-sm-2 control-label">Nội dung:</label>
								<div class="col-sm-6">
									<textarea class="form-control" name="message" rows="5"></textarea>
								</div>
							</div>
						
							<div class="form-group">
								<div class="col-sm-10 col-sm-offset-2">
									<button type="submit" class="btn btn-primary">Gửi yêu cầu</button>
								</div>
							</div>
	
						</form>
						{include file="$view_dir/layouts/footer.tpl"}