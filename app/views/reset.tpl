{include file="$view_dir/layouts/header.tpl"}
					<h3>Xác lập lại mật khẩu</h3>
						
						<hr>
						<form action="" method="POST" class="form-horizontal" role="form">
							{include file="$view_dir/layouts/frontend-response.tpl"}
							<div class="form-group">
								<label for="inputCode" class="col-sm-2 control-label">Reset Code:</label>
								<div class="col-sm-6">
									<input type="text" name="code" id="inputCode" class="form-control" value="{if isset($code)}{$code}{/if}" required="required" disabled="disabled" title="">
								</div>
							</div>
							<div class="form-group">
								<label for="inputNewpass" class="col-sm-2 control-label">Mật khẩu mới:</label>
								<div class="col-sm-6">
									<input type="password" name="newpass" id="inputNewpass" class="form-control" required="required" title="">
								</div>
							</div>
							<div class="form-group">
								<label for="inputConfirm" class="col-sm-2 control-label">Nhập lại:</label>
								<div class="col-sm-6">
									<input type="password" name="confirm" id="inputConfirm" class="form-control" required="required" title="">
								</div>
							</div>
						
							<div class="form-group">
								<div class="col-sm-6 col-sm-offset-2">
									<button type="submit" class="btn btn-primary">Đổi mật khẩu</button>
								</div>
							</div>
	
						</form>
						<div class="col-sm-6 col-xs-offset-2">
							<p><a href="{$asset}login.html">Đăng nhập</a> / <a href="{$asset}register.html">Đăng ký</a></p>
						</div>
{include file="$view_dir/layouts/footer.tpl"}