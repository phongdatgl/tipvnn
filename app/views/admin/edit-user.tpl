{include file="$view_dir/layouts/header_admin.tpl"}

<form action="" method="POST" class="form-horizontal" role="form">
		{include file="$view_dir/layouts/frontend-response.tpl"}
		<div class="form-group">
			<label for="username" class="col-sm-2 control-label">Username:</label>
			<div class="col-sm-6">
				<input type="text" name="username" id="username" class="form-control" value="{$u->username}" required="required" title="">
			</div>
		</div>
		<div class="form-group">
			<label for="email" class="col-sm-2 control-label">Email:</label>
			<div class="col-sm-6">
				<input type="email" name="email" id="email" class="form-control" value="{$u->email}" required="required" title="">
			</div>
		</div>
		<div class="form-group">
			<label for="password" class="col-sm-2 control-label">Password:</label>
			<div class="col-sm-6">
				<input type="password" name="password" id="password" class="form-control" value="" placeholder=""  title="">
			</div>
		</div>
		<div class="form-group">
			<label for="phone" class="col-sm-2 control-label">Phone:</label>
			<div class="col-sm-6">
				<input type="text" name="phone" id="phone" class="form-control" value="{$u->phone}" title="">
			</div>
		</div>
		<div class="form-group">
			<label for="balance" class="col-sm-2 control-label">Tiền:</label>
			<div class="col-sm-2">
				<div class="input-group">
				  	<input type="text" name="balance" id="balance" class="form-control" value="{$u->balance}" title="">
				  	<span class="input-group-addon">$</span>
				</div>
				
			</div>
		</div>
		<div class="form-group">
			<label for="activated" class="col-sm-2 control-label">Trạng thái:</label>
			<div class="col-sm-2">
				<select name="activated" id="activated" class="form-control" required="required">
					<option value="1" {if $u->activated==1}selected{/if}>Kích hoạt</option>
					<option value="0" {if $u->activated==0}selected{/if}>Bị khoá</option>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label for="group" class="col-sm-2 control-label">User Group:</label>
			<div class="col-sm-2">
				<select name="groups" id="groups" class="form-control" required="required">
					<option value="4" {if in_array(4, $u->groups)}selected{/if}>User</option>
					<option value="5" {if in_array(5, $u->groups)}selected{/if}>Test Tipser</option>
					<option value="3" {if in_array(3, $u->groups)}selected{/if}>Free Tipser</option>
					<option value="2" {if in_array(2, $u->groups)}selected{/if}>VIP Tipser</option>
					<option value="6" {if in_array(6, $u->groups)}selected{/if}>Tip Admin</option>
					<option value="1" {if in_array(1, $u->groups)}selected{/if}>Administrator</option>
					
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</div>
</form>

{include file="$view_dir/layouts/footer.tpl"}