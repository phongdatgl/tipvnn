<?php 
use Symfony\Component\HttpFoundation\Response;
use Cartalyst\Sentry\Users\Eloquent\User;
class AuthenController {
	public function login(Application $app) 
	{
		$form = $app['session']->getFlashBag()->get('formValue');
		
		$app['smarty']->assign('form', @$form[0]);
		$app['smarty']->assign('title', 'Đăng nhập');
		return $app['smarty']->display('login.tpl');
	}
	public function doLogin(Application $app)
	{
		$referer = $app['request']->get('redirect_to');
		if($referer == NULL) $referer = '/';
		try
		{
		    // Login credentials
		    $credentials = array(
		        'username'    => $_REQUEST['username'],
		        'password' => $_REQUEST['password'],
		    );
		    if(isset($_REQUEST['username'])) $remember = true;
		    else $remember = false;
		    // Authenticate the user
		    $user = Sentry::authenticate($credentials, $remember);

		    return $app->redirect($referer, 301);
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		    $app['session']->getFlashBag()->add('error', 'Bạn chưa nhập tên đăng nhập');
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
		    $app['session']->getFlashBag()->add('error', 'Mật khẩu không được bỏ trống');
		}
		catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
		{
		    $app['session']->getFlashBag()->add('error', 'Tên đăng nhập hoặc mật khẩu không đúng');
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    $app['session']->getFlashBag()->add('error', 'Tên đăng nhập không tồn tại');
		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
		    $app['session']->getFlashBag()->add('error', 'Tài khoản của bạn chưa được kích hoạt');
		}
	 	$app['session']->getFlashBag()->add('formValue', array('username'=>$_REQUEST['username']));
		return $app->redirect('/login.html?redirect_to=' . urlencode($referer), 301);
	}
	public function createCaptcha(Application $app)
	{
		$security_code = rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
		$app['session']->set('tip_sec_code', $security_code);
		$width = 65;
		$height = 32;
		$image = ImageCreate($width, $height);
		$white = ImageColorAllocate($image, 255, 255, 255);
		$bg = ImageColorAllocate($image, 221, 221, 221);
		$black = ImageColorAllocate($image, 0, 0, 0);
		ImageFill($image, 0, 0, $bg);
		ImageString($image, 5, 10, 6, $security_code, $black);
		ImageJpeg($image);
		$img = ob_get_contents();
		ob_end_clean();
		ImageDestroy($image);
		return new Response($img, 200, ['Content-Type' => 'image/jpeg']);
	}
	public function register (Application $app)
	{
		$form = $app['session']->getFlashBag()->get('formValue');
		
		$app['smarty']->assign('form', @$form[0]);
		$app['smarty']->assign('title', 'Đăng kí tài khoản');
		return $app['smarty']->display('register.tpl');
	}
	public function doRegister (Application $app)
	{
		extract($_REQUEST);
		$app['session']->getFlashBag()->add('formValue', array('username'=>$username, 'email'=>$email,'phone'=>$phone));
		if(isset($username) && isset($password) && isset($confirm) && isset($email)) {
			if(strlen($username) < 3 || strlen($username) > 50) {
				$app['session']->getFlashBag()->add('error', 'Tên đăng nhập quá dài');
			} elseif($password != $confirm) {
				$app['session']->getFlashBag()->add('error', 'Nhập lại mật khẩu không đúng');
			} elseif($app['session']->get('tip_sec_code') != $captcha) {
				$app['session']->getFlashBag()->add('error', 'Mã captcha không chính xác');
			} else {
				$ck = DB::table('users')->where('email', '=', $email)->first();
				if(count($ck)) {
					$app['session']->getFlashBag()->add('error', 'Email đã tồn tại');
				} else {
					try {
						switch ($app['request']->get('user_type')) {
							case 1:
								$group_id = 4; $activated = true;
								break;
							case 2:
								$group_id = 3; $activated = false;
								break;
							case 3:
								$group_id = 2; $activated = false;
								break;
							case 4:
								$group_id = 5; $activated = false;
								break;
							default:
								$group_id = 4; $activated = true;
								break;
						}
					    // Let's register a user.
					    $user = Sentry::register(array(
					        'email'    => $email,
					        'username'    => $username,
					        'password' => $password,
					        'activated'=> $activated
					    ));
					    $group = Sentry::findGroupById($group_id);
					    $user->addGroup($group);
					    $user->phone = $phone;
					    $user->save();
					    if($group_id == 4) $app['session']->getFlashBag()->add('success', 'Đăng ký thành công. Vui lòng đăng nhập');
					    else $app['session']->getFlashBag()->add('success', 'Đăng ký thành công. Tài khoản của bạn chưa được kích hoạt. Vui lòng liên hệ tới Quản Trị viên');
					    $app['session']->getFlashBag()->add('formValue', array());
					}
					catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
					{
					    $app['session']->getFlashBag()->add('error', 'Tên tài khoản không được bỏ trống');
					}
					catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
					{
					    $app['session']->getFlashBag()->add('error', 'Mật khẩu không được bỏ trống');
					}
					catch (Cartalyst\Sentry\Users\UserExistsException $e)
					{
					    $app['session']->getFlashBag()->add('error', 'Username đã tồn tại');
					}
				}
			}
			
			return $app->redirect('/register.html', 301);
		}
	}
	public function forgot(Application $app)
	{
		
		$app['smarty']->assign('title', 'Quên mật khẩu');
		return $app['smarty']->display('forgot.tpl');
	}
	public function doForgot(Application $app)
	{
		global $set, $mailConfig;
		extract($_REQUEST);
		try
		{
		    // Find the user using the user email address
		    User::setLoginAttributeName('email');
		    $user = Sentry::findUserByLogin($email);

		    // Get the password reset code
		    $resetCode = $user->getResetPasswordCode();
		    $mess = 'Vui lòng bấm vào link để xác lập lại mật khẩu của bạn : ' . 
		    		$set["site_url"] . 'confirm-forgot/' . $resetCode . '.html' ;
		    $message = \Swift_Message::newInstance()
		        ->setSubject('['.$set["site_name"].'] Quên mật khẩu')
		        ->setFrom(array($mailConfig['username']=>$set['site_name']))
		        ->setTo(array($user->email=>$user->username))
		        ->setBody($mess);

		    $app['mailer']->send($message);
		   $app['session']->getFlashBag()->add('success', 'Chúng tôi vừa gửi cho bạn 1 email xác lập lại mật khẩu. Vui lòng check mail và làm theo hướng dẫn');
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		   $app['session']->getFlashBag()->add('error', 'Email không tồn tại');
		}
		return $app->redirect('/forgot.html', 301);
	}
	public function reset(Application $app, $code)
	{
		$app['smarty']->assign('title', 'Thiết lập mật khẩu mới');
		
		try
		{
		    $user = Sentry::findUserByResetPasswordCode($code);
		    $app['smarty']->assign('code', $code);

		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			if( ! $success)
		    	$app['session']->getFlashBag()->add('error', 'Mã code không tồn tại');
		    
		}
		
		return $app['smarty']->display('reset.tpl');
		
	}
	public function doReset(Application $app, $code)
	{
		if($app['request']->get('newpass') != $app['request']->get('confirm')) 
		{
			$app['session']->getFlashBag()->add('error', 'Nhập lại mật khẩu không chính xác.');
		} 
		elseif(strlen($app['request']->get('newpass')) < 4) 
		{
			$app['session']->getFlashBag()->add('error', 'Độ dài mật khẩu phải lớn hơn 4 kí tự.');
		} else {
			try
			{
		    	$user = Sentry::findUserByResetPasswordCode($code);
			    $user->password = $app['request']->get('newpass');
			    $user->reset_password_code = NULL;
			    if($user->save())
			    {
			    	$app['session']->getFlashBag()->add('success', 'Cập nhật mật khẩu thành công');
			    } else {
			    	$app['session']->getFlashBag()->add('error', 'Cập nhật mật khẩu thất bại. Vui lòng liên hệ với webmaster');
			    }
			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
			    $app['session']->getFlashBag()->add('error', 'Mã code không tồn tại');
			}
		}
		return $app->redirect('/confirm-forgot/'.$code.'.html', 301);
	}

	public function profile(Application $app)
	{
		$app['smarty']->assign('title', 'Thông tin tài khoản');
		$user = Sentry::getUser();
		$app['smarty']->assign('user', $user);
		
		return $app['smarty']->display('profile.tpl');
	}
	public function doUpdateProfile(Application $app)
	{
		$user = Sentry::getUser();
		$regex = "/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/"; 
		if(preg_match($regex, $app['request']->get('email')))
		{
			$user->email = $app['request']->get('email');
			$user->phone = $app['request']->get('phone');
			if($user->save())
			{
				$app['session']->getFlashBag()->add('success', 'Cập nhật thông tin thành công');
			} else {
				$app['session']->getFlashBag()->add('success', 'Có lỗi khi cập nhật thông tin');
			}
		} else {
			$app['session']->getFlashBag()->add('error', 'Email không hợp lệ.');
		}
		return $app->redirect('/profile.html', 301);

	}
	public function changepass(Application $app)
	{
		
		$app['smarty']->assign('title', 'Đổi mật khẩu');
		return $app['smarty']->display('changepass.tpl');
	}
	public function doChangepass(Application $app)
	{
		if($app['request']->get('newpass') != $app['request']->get('confirm')) {
			$app['session']->getFlashBag()->add('error', 'Nhập lại mật khẩu không đúng.');
		} elseif(strlen($app['request']->get('newpass')) < 4) {
			$app['session']->getFlashBag()->add('error', 'Mật khẩu quá ngắn. Độ dài mật khẩu phải lớn hơn 4 ký tự');
		} else {
			$user = Sentry::getUser();
			if($user->checkPassword($app['request']->get('crpassword'))) {
				$user->password = $app['request']->get('newpass');
				if($user->save()) {
					$app['session']->getFlashBag()->add('success', 'Cập nhật mật khẩu mới thành công.');
				} else {
					$app['session']->getFlashBag()->add('error', 'Có lỗi khi cập nhật mật khẩu.');
				}
			} else {
				$app['session']->getFlashBag()->add('error', 'Mật khẩu cũ không chính xác.');
			}
		}
		return $app->redirect('/change-pass.html', 301);
	}
}
?>