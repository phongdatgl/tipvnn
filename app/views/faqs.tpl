						{include file="$view_dir/layouts/header.tpl"}
						<h3>Chào mừng bạn đến với {$setting.site_name}</h3>
						<hr>
						<div class="col-sm-6">
							<h4>TOP TIPSER</h4>
							<table class="table table-hover table-bordered">
								<thead>
									<tr>
										<th>#</th>
										<th>User Name</th>
										<th>Picked</th>
										<th>WIN</th>
										<th>LOSE</th>
										<th>DRAW</th>
									</tr>
								</thead>
								<tbody>
								{$stt = 1}
									{foreach $topTipser as $v}
									<tr>
										<td>{$stt}</td>
										<td>{$v.username}</td>
										<td>{$v.win + $v.lose + $v.draw}</td>
										<td>{$v.win}</td>
										<td>{$v.lose}</td>
										<td>{$v.draw}</td>
									</tr>
									{$stt  = $stt + 1}
									{/foreach}
								</tbody>
							</table>
						</div>
						<div class="col-sm-6">
							<h4>TOP TIPSER {date('m/Y')}</h4>
							<table class="table table-hover table-bordered">
								<thead>
									<tr>
										<th>#</th>
										<th>User Name</th>
										<th>Picked</th>
										<th>WIN</th>
										<th>LOSE</th>
										<th>DRAW</th>
									</tr>
								</thead>
								<tbody>
								{$stt = 1}
									{foreach $topMonthTipser as $v}
									<tr>
										<td>{$stt}</td>
										<td>{$v.username}</td>
										<td>{$v.win + $v.lose + $v.draw}</td>
										<td>{$v.win}</td>
										<td>{$v.lose}</td>
										<td>{$v.draw}</td>
									</tr>
									{$stt  = $stt + 1}
									{/foreach}
								</tbody>
							</table>
						</div>
						<div class="clearfix"></div>
						<hr>
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						  	{foreach $faqs as $k=>$v}
						  	<div class="panel panel-primary">
						    	<div class="panel-heading" role="tab" id="heading{$k}">
						     		<h4 class="panel-title">
						        		<a data-toggle="collapse" data-parent="#accordion" href="#collapse{$k}" aria-expanded="true" aria-controls="collapse{$k}">
						          		{$v.page_name}
						        		</a>
						      		</h4>
						    	</div>
							    <div id="collapse{$k}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{$k}">
							      	<div class="panel-body">
							        	{$v.page_content}
							      	</div>
							    </div>
						  	</div> <!-- end .panel -->
						  	{/foreach}
						</div>
						{include file="$view_dir/layouts/footer.tpl"}