<?php 
namespace Core;
class View {
	public static function make($template_name, $data = array()) {
		global $application_folder;
		include_once APPPATH . '/' . $application_folder . '/views/' . $template_name . '.php';
		return '';
	}
}
?>