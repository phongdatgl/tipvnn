<?php 
use Symfony\Component\HttpFoundation\Response;
class AdminController {
	public function index (Application $app)
	{
		$app['smarty']->assign('title', 'Administrator');
		return $app['smarty']->display('admin/index.tpl');
	}
	public function tiplist(Application $app, $page = 1)
	{
		$app['smarty']->assign('title', 'Tip Manager');
		$limit = 10;
		$offset = $page * $limit - $limit;
		$app['smarty']->assign('stt', $offset + 1);
		$totalTips = count(Tiplist::getAllTips());
		$totalPage = ceil($totalTips/$limit);
		$link = ADMIN_DIR . '/tiplist-%s.html';
		$pagination = Pagination::init($totalPage, $page, $link, false);
		$app['smarty']->assign('pagination', $pagination);
		$tiplist = Tiplist::getAllTips($offset, $limit);
		$app['smarty']->assign('tiplist', $tiplist);
		return $app['smarty']->display('admin/tiplist.tpl');
	}
	public function tipEdit(Application $app, $id)
	{
		$tipInfo = Tiplist::getTipInfo($id);
		if( ! count($tipInfo)) {
			return new Response('Không tìm thấy TIP', 404 /* ignored */, array('X-Status-Code' => 200, 'Content-Type'=>'text/html'));
		}

		$app['smarty']->assign('title', 'Edit Tip');
		$app['smarty']->assign('tip', $tipInfo);
		return $app['smarty']->display('admin/edit-tip.tpl');
	}
	public function doTipEdit(Application $app, Request $request, $id) {
		$tipInfo = Tiplist::getTipInfo($id);
		if( ! count($tipInfo)) {
			return new Response('Không tìm thấy TIP', 404 /* ignored */, array('X-Status-Code' => 200, 'Content-Type'=>'text/html'));
		}
		
		$user = Sentry::getUser();
		$type = $app['request']->get('type');
		$arr = array("\'", '\"', "\\");
			if( ! $user->hasAccess('admin')) {
				if( ! $user->hasAccess('uptipvip')) {
					if($user->hasAccess('uptipfree')) {
						$type = 0;
					} elseif($user->hasAccess('testtip')) {
						$type = 2;
					}
				}
			}

		$info = $app['request']->get('info');
		$info = str_replace($arr, '', strip_tags($info, '<a><p><img>'));
		$data = array(
			'start_time'	=>	$app['request']->get('start_time'),
			'league'	=>	$app['request']->get('league'),
			'home'	=>	$app['request']->get('home'),
			'odds'	=>	$app['request']->get('odds'),
			'away'	=>	$app['request']->get('away'),
			'tips'	=>	$app['request']->get('tips'),
			'score'	=>	$app['request']->get('score'),
			'type'	=>	$type,
			'isvip'	=>	(($type<3)?$type:1),
			'price'	=>	(($type<3)?0:$app['request']->get('price')),
			'result'=>	$app['request']->get('result'),
			'info'	=>	$info
			);
		if(Tiplist::updateTip($data, $id)) {
			$app['session']->getFlashBag()->add('success', 'Cập nhật thành công !');
		} else {
			$app['session']->getFlashBag()->add('error', 'Có lỗi khi cập nhật');
		}
		//update refund
		if($app['request']->get('result') > 0) {
			$this->doTipMoneyAction($app, $tipInfo['id'], $app['request']->get('result'));
		}
		
		return $app->redirect(ADMIN_DIR . '/edit-tip-'.$id.'.html', 301);
	}
	public function tipAdd(Application $app)
	{
		$app['smarty']->assign('title', 'Thêm TIP');
		$data = array(
			'start_time'	=>	date('Y-m-d H:i'),
			'league'	=>	'',
			'home'	=>	'',
			'odds'	=>	'',
			'away'	=>	'',
			'tips'	=>	'',
			'score'	=>	'',
			'type'	=>	0,
			'price'	=>	0,
			'result'=>	0,
			'info'	=>	''
			);
		$app['smarty']->assign('tip', $data);
		return $app['smarty']->display('admin/edit-tip.tpl');
	}
	public function doTipAdd(Application $app)
	{
		if(strtotime($app['request']->get('start_time') . ':00') < (time()+1800) ) {
			$app['session']->getFlashBag()->add('error', 'Bạn chỉ được add tip của trận đấu chưa diễn ra !');
		} else {
			$user = Sentry::getUser();
			$type = $app['request']->get('type');
			if( ! $user->hasAccess('admin')) {
				if( ! $user->hasAccess('uptipvip')) {
					if($user->hasAccess('uptipfree')) {
						$type = 0;
					} elseif($user->hasAccess('testtip')) {
						$type = 2;
					}
				}
			}
			$arr = array("\'", '\"', "\\");
			if(Tiplist::checkAddTip($user, $app['request']->get('start_time'))) {
				$info = $app['request']->get('info');
				$info = str_replace($arr, '', strip_tags($info, '<a><p><img>'));
				$data = array(
					'start_time'	=>	$app['request']->get('start_time'),
					'league'	=>	$app['request']->get('league'),
					'home'	=>	$app['request']->get('home'),
					'odds'	=>	$app['request']->get('odds'),
					'away'	=>	$app['request']->get('away'),
					'tips'	=>	$app['request']->get('tips'),
					'score'	=>	$app['request']->get('score'),
					'type'	=>	$type,
					'isvip'	=>	(($type<3)?$type:1),
					'price'	=>	(($type<3)?0:$app['request']->get('price')),
					'result'=>	$app['request']->get('result'),
					'info'	=>	$info
					);
				if(Tiplist::insertTip($data)) {
					$app['session']->getFlashBag()->add('success', 'Thêm tip thành công !');
				} else {
					$app['session']->getFlashBag()->add('error', 'Có lỗi khi thêm');
				}
			} else {
				$app['session']->getFlashBag()->add('error', 'Bạn chỉ được add 2 tip/ngày');
			}
			
		}
		return $app->redirect(ADMIN_DIR . '/add-tip.html', 301);
	}
	public function userlist(Application $app, $page = 1)
	{
		$limit = 10;
		if($app['request']->get('keyword') !== NULL) {
			$keyword = $app['request']->get('keyword');

		} else {
			$keyword = '';
		}$app['smarty']->assign('keyword', $keyword);
		$offset = $page * $limit - $limit;
		$app['smarty']->assign('stt', $offset + 1);
		$totalUser = count(User::getUsers(false, false, $keyword));
		$totalPage = ceil($totalUser/$limit);
		$link = ADMIN_DIR . '/users-%s.html';
		$pagination = Pagination::init($totalPage, $page, $link, false);
		$app['smarty']->assign('pagination', $pagination);

		$users = User::getUsers($limit, $offset, $keyword);
		$app['smarty']->assign('users', $users);
		$app['smarty']->assign('title', 'User Manager');

		return $app['smarty']->display('admin/users.tpl');
	}
	public function addUser(Application $app)
	{
		$user = array(
			'username'=>'',
			'email'=>'',
			'phone'=>'',
			'balance'=>0,
			'activated'=>1,
			'groups'=>array(),
			);
		$user = json_decode(json_encode($user));
		$app['smarty']->assign('u', $user);
		return $app['smarty']->display('admin/edit-user.tpl');
	}
	public function doAddUser(Application $app)
	{
		$username = $app['request']->get('username');
		$email = $app['request']->get('email');
		if( ! User::checkUserExist($username, $email)) {
			$app['session']->getFlashBag()->add('error', 'Username hoặc Email đã tồn tại !');
		} else {
			if($app['request']->get('activated') == 1) $activated = true;
			else $activated = false;
			$udata = array(
				'username'=>$app['request']->get('username'),
				'email'=>$app['request']->get('email'),
				'password'=>$app['request']->get('password'),
				'phone'=>$app['request']->get('phone'),
				'balance'=>$app['request']->get('balance'),
				'activated'=>$activated
				);
			try {
				$user = Sentry::createUser($udata);
				$group = Sentry::findGroupById($app['request']->get('groups'));
				$user->addGroup($group);
				$app['session']->getFlashBag()->add('success', 'Thêm User thành công !');
			} catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
			   $app['session']->getFlashBag()->add('error', 'Username không được trống !');
			} catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
			   $app['session']->getFlashBag()->add('error', 'Password không được trống !');
			} catch (Cartalyst\Sentry\Users\UserExistsException $e) {
			    $app['session']->getFlashBag()->add('error', 'Username hoặc Email đã tồn tại !');
			} catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
			    $app['session']->getFlashBag()->add('error', 'Group không tìm thấy !');
			}
		}
		
		return $app->redirect(ADMIN_DIR . '/add-user.html', 301);
	}
	public function editUser(Application $app, $id)
	{
		try {
			$user = Sentry::findUserById($id);
		} catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
		    return new Response('Không tìm thấy User', 404 /* ignored */, array('X-Status-Code' => 200, 'Content-Type'=>'text/html'));
		}
		$user->groups = array($user->groups[0]->id);
		$app['smarty']->assign('u', $user);
		return $app['smarty']->display('admin/edit-user.tpl');
	}
	public function doEditUser(Application $app, $id)
	{
		try {
			$user = Sentry::findUserById($id);
		} catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
		    return new Response('Không tìm thấy User', 404 /* ignored */, array('X-Status-Code' => 200, 'Content-Type'=>'text/html'));
		}
		$currentGroup = $user->groups[0]->id;
		$uGroup = Sentry::findGroupById($currentGroup);
		$user->username = $app['request']->get('username');
		$user->email = $app['request']->get('email');
		if(strlen($app['request']->get('password'))>0) $user->password = $app['request']->get('password');
		$user->phone = $app['request']->get('phone');
		$user->balance = $app['request']->get('balance');
		$user->activated = $app['request']->get('activated');
		if($user->save()) {
			$app['session']->getFlashBag()->add('success', 'Cập nhật thông tin user thành công');
		} else {
			$app['session']->getFlashBag()->add('error', 'Cập nhật thông tin user bị lỗi');
		}
		if($currentGroup != $app['request']->get('groups')) {
			$newGroup = Sentry::findGroupById($app['request']->get('groups'));
			$user->removeGroup($uGroup);
			$user->addGroup($newGroup);
		}
		return $app->redirect(ADMIN_DIR . '/users.html', 301);
	}
	public function deleteUser(Application $app, $id) 
	{
		try {
		    // Find the user using the user id
		    $user = Sentry::findUserById($id);

		    // Delete the user
		    $user->delete();
		    $app['session']->getFlashBag()->add('success', 'Xoá thành công !');
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    $app['session']->getFlashBag()->add('error', 'Có lỗi khi xoá user !');
		}
		return $app->redirect(ADMIN_DIR . '/users.html', 301);
	}
	public function settings(Application $app) 
	{
		$app['smarty']->assign('title', 'Cài đặt website');
		$app['smarty']->assign('sets', Setting::getSettings());
		return $app['smarty']->display('admin/settings.tpl');
	}
	public function doSaveSettings(Application $app)
	{
		if(Setting::saveSettings($_REQUEST)) {
			$app['session']->getFlashBag()->add('success', 'Lưu cài đặt thành công !');
		} else {
			$app['session']->getFlashBag()->add('error', 'Có lỗi khi lưu !');
		}
		return $app->redirect(ADMIN_DIR . '/settings.html', 301);
	}
	public function doTipMoneyAction(Application $app, $id, $isWin)
	{
		global $set;
		$tip = Tiplist::getTipById($id);
		if( ! count($tip)) return '';
		if($tip['is_refund'] == 1) return '';
		if($isWin == 1) { //tip win, cong tien vao thang tipser
			$tipser = $tip['user_id'];
			$totalMoneyBought = Transaction::getTipBoughtByTipId($id);
			if( $totalMoneyBought != NULL && $totalMoneyBought > 0) {
				$money = ceil($totalMoneyBought * ($set['aff_percent']/100));
				$data = array(
					'code'		=>	transaction_code_generator(),
					'create_at'	=>	date('Y-m-d H:i:s'),
					'total'		=>	$money,
					'user_id'	=>	$tipser,
					'type'		=>	1,
					'status'	=>	2,
					'meta'		=>	array(
									'aff_type'		=>	'Trả tiền hoa hồng cho Tipser',
									'aff_total'		=>	$totalMoneyBought . '$',
									'aff_tran'		=>	"Trận ".$tip['home']." vs ".$tip['away'],
									'aff_percent'	=>	$set['aff_percent'] . '%',
									'aff_time'		=>	date('Y-m-d H:i:s')
									)
					);
				$ck = Tiplist::updateTip(array('is_refund'=>1), $id);
				if($ck) {
					Transaction::insertNewTransaction($data);
					try {
						$u = Sentry::findUserById($tipser);
						$u->balance = $u->balance + $money;
						$u->save();
						return '';
					}catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
						return '';
					}
					
				}
				
			}
		} elseif($isWin === 2) { //tip thua, cong tien vao thang mua tip
			$lists = Transaction::getUsersBought($id);
			$data = array();
			if(count($lists)) {
				foreach ($lists as $value) {
					$data[] = array('user' => $value['user_id'], 'total'	=>	ceil($value['total'] * ($set['refund_percent']/100)), 'bought'=>$value['total']);
				}
				//Update user Balance 
				User::refundTip($data);
				foreach ($data as $key => $value) {
					$data = array(
						'code'		=>	transaction_code_generator(),
						'create_at'	=>	date('Y-m-d H:i:s'),
						'total'		=>	$value['total'],
						'user_id'	=>	$value['user'],
						'type'		=>	1,
						'status'	=>	2,
						'meta'		=>	array(
										'ref_type'		=>	'Hoàn trả tiền mua TIP (thua)',
										'ref_bought'	=>	$value['bought'] . "$",
										'ref_receive'	=>	"Nhận lại ".$value['total'] . "$",
										'ref_percent'	=>	$set['refund_percent'] . '%',
										'ref_time'		=>	date('Y-m-d H:i:s')
										)
						);
					Transaction::insertNewTransaction($data);
				}
				$ck = Tiplist::updateTip(array('is_refund'=>1), $id);
				return '';
			} 
			return '';
		} else {//hoa, refund 50%
			$lists = Transaction::getUsersBought($id);
			$data = array();
			$drawRefund = 50;
			if(count($lists)) {
				foreach ($lists as $value) {
					$data[] = array('user' => $value['user_id'], 'total'	=>	ceil($value['total'] * ($drawRefund/100)), 'bought'=>$value['total']);
				}
				//Update user Balance 
				User::refundTip($data);
				foreach ($data as $key => $value) {
					$data = array(
						'code'		=>	transaction_code_generator(),
						'create_at'	=>	date('Y-m-d H:i:s'),
						'total'		=>	$value['total'],
						'user_id'	=>	$value['user'],
						'type'		=>	1,
						'status'	=>	2,
						'meta'		=>	array(
										'ref_type'		=>	'Hoàn trả tiền mua TIP (thua)',
										'ref_bought'	=>	$value['bought'] . "$",
										'ref_receive'	=>	"Nhận lại ".$value['total'] . "$",
										'ref_percent'	=>	$drawRefund . '%',
										'ref_time'		=>	date('Y-m-d H:i:s')
										)
						);
					Transaction::insertNewTransaction($data);
				}
				$ck = Tiplist::updateTip(array('is_refund'=>1), $id);
				return '';
			} 
			return '';
		}
	}
	public function transactions(Application $app, $page = 1)
	{
		$app['smarty']->assign('title', 'Lịch sử giao dịch');
		if($app['request']->get('keyword') !== NULL) {
			$keyword = $app['request']->get('keyword');

		} else {
			$keyword = '';
		}$app['smarty']->assign('keyword', $keyword);
		if(isset($_REQUEST['status'])) $status = $_REQUEST['status'];
		else $status = 'all';
		$limit = 10;
		$offset = $page * $limit - $limit;
		$app['smarty']->assign('stt', $offset + 1);
		$totalRow = count(Transaction::getTransactionList(false, false, false, $keyword, $status));
		$totalPage = ceil($totalRow/$limit);
		$link = ADMIN_DIR . '/transactions-%s.html?keyword='.$keyword;
		$pagination = Pagination::init($totalPage, $page, $link, false);
		$app['smarty']->assign('pagination', $pagination);
		$transactions = Transaction::getTransactionList(false, $limit, $offset, $keyword, $status);
		$app['smarty']->assign('transactions', $transactions);
		return $app['smarty']->display('admin/transactions.tpl');
	}
	public function viewTransaction(Application $app, $id)
	{
		$trans = Transaction::adminViewTrans($id);
		if( ! count($trans)) {
			return new Response('Không tìm thấy Transaction. Bấm vào <a href="/index.html">đây</a> để về trang chủ', 404 /* ignored */, array('X-Status-Code' => 200, 'Content-Type'=>'text/html'));
		}
		$app['smarty']->assign('title', 'Xem chi tiết giao dịch #' . $trans['code']);
		$app['smarty']->assign('trans', $trans);
		return $app['smarty']->display('admin/view-trans.tpl');

	}
	public function viewPage(Application $app)
	{
		$app['smarty']->assign('title', 'Quản lý trang');
		$pages = Pages::getPage(2);
		$app['smarty']->assign('stt', 1);
		$app['smarty']->assign('pages', $pages);
		return $app['smarty']->display('admin/pages.tpl');
	}
	public function editPage(Application $app, $id)	
	{
		$page = Pages::getPageById($id);
		if( ! count($page)) {
			return new Response('Không tìm thấy Page. Bấm vào <a href="'.ADMIN_DIR.'/pages.html">đây</a> để trở lại', 404 /* ignored */, array('X-Status-Code' => 200, 'Content-Type'=>'text/html'));
		}
		$app['smarty']->assign('title', 'Edit page #' . $page['id']);
		$app['smarty']->assign('page', $page);
		return $app['smarty']->display('admin/edit-page.tpl');

	}
	public function doEditPage(Application $app, $id)	
	{
		$page = Pages::getPageById($id);
		if( ! count($page)) {
			return new Response('Không tìm thấy Page. Bấm vào <a href="'.ADMIN_DIR.'/pages.html">đây</a> để trở lại', 404 /* ignored */, array('X-Status-Code' => 200, 'Content-Type'=>'text/html'));
		}
		$data = array('page_name'=>$app['request']->get('page_name'), 'page_content'=>$app['request']->get('page_content'),'sortorder'=>$app['request']->get('sortorder'));
		if(Pages::updatePage($data, $id)) {
			$app['session']->getFlashBag()->add('success', 'Lưu thay đổi thành công !');
		} else {
			$app['session']->getFlashBag()->add('error', 'Có lỗi khi lưu !');
		}
		return $app->redirect(ADMIN_DIR . '/pages.html', 301);
	}
	public function addPage(Application $app)
	{
		$app['smarty']->assign('title', 'Add Page');
		$page = array('page_name'=>'', 'page_content'=>'', 'sortorder'=>0);
		$app['smarty']->assign('page', $page);
		return $app['smarty']->display('admin/edit-page.tpl');
	}
	public function doAddPage(Application $app)
	{
		
		$data = array('page_name'=>$app['request']->get('page_name'), 'page_content'=>$app['request']->get('page_content'),'sortorder'=>$app['request']->get('sortorder'), 'page_type'=>2);
		if(Pages::insertNewPage($data)) {
			$app['session']->getFlashBag()->add('success', 'Thêm page mới thành công !');
		} else {
			$app['session']->getFlashBag()->add('error', 'Có lỗi khi add page !');
		}
		return $app->redirect(ADMIN_DIR . '/pages.html', 301);
	}
	public function saveTransaction(Application $app)
	{
		$ck = Transaction::saveStatus($app['request']->get('id'),$app['request']->get('status'));
		if($ck) {
			if($app['request']->get('status') == 2) { 
				$trans = Transaction::adminViewTrans($app['request']->get('id'));
				$check = false;
				foreach ($trans['meta'] as $key => $value) {
					if($value['name'] == 'Số tài khoản') {
						$check = true;
						break;
					}
				}
				if($check) {
					$u = Sentry::findUserById($trans['user_id']);
					$u->balance = $u->balance + $trans['total'];
					$u->save();
				}
			}
			
			
		}
		return 'Đã lưu thay đổi';
	}
}
?>