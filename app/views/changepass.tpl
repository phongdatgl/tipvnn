{include file="$view_dir/layouts/header.tpl"}
					<h3>Đổi mật khẩu</h3>
						<hr>
						<form action="" method="POST" class="form-horizontal" role="form">
							{include file="$view_dir/layouts/frontend-response.tpl"}
							
							<div class="form-group">
								<label for="inputCrpassword" class="col-sm-2 control-label">Mật khẩu hiện tại:</label>
								<div class="col-sm-6">
									<input type="password" name="crpassword" id="inputCrpassword" class="form-control" required="required" title="">
								</div>
							</div>
							<div class="form-group">
								<label for="inputNewpass" class="col-sm-2 control-label">Mật khẩu mới:</label>
								<div class="col-sm-6">
									<input type="password" name="newpass" id="inputNewpass" class="form-control" required="required" title="">
								</div>
							</div>
							<div class="form-group">
								<label for="inputConfirm" class="col-sm-2 control-label">Nhập lại:</label>
								<div class="col-sm-6">
									<input type="password" name="confirm" id="inputConfirm" class="form-control" required="required" title="">
								</div>
							</div>
								<div class="form-group">
									<div class="col-sm-10 col-sm-offset-2">
										<button type="submit" class="btn btn-primary">Đổi mật khẩu</button>
									</div>
								</div>
						</form>
{include file="$view_dir/layouts/footer.tpl"}