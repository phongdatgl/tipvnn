<?php 
set_time_limit(0);
use Cartalyst\Sentry\Users\Eloquent\User;
use Cartalyst\Sentry\Users\Eloquent\Provider as UserProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Silex\Application;
use FractalizeR\Smarty\ServiceProvider as SmartyServiceProvider;

// Load các vendor của composer
include_once APPPATH . "/vendor/autoload.php";

//Tên thư mục chứa ứng dụng
$application_folder = 'app';
define ('FCPATH', APPPATH . '/' . $application_folder ); 
//Load các config trong thư mục application , config
$path = APPPATH . '/' . $application_folder . '/config';
$dh  = opendir($path);
while (false !== ($filename = readdir($dh))) {
	if(!in_array($filename,array(".",".."))) 
		require_once $path . '/' . $filename;
}

//Tạo kết nối tới database
$capsule = new Illuminate\Database\Capsule\Manager;
$capsule->addConnection($config);
$capsule->setAsGlobal();
$capsule->bootEloquent();
//Load Twig

//Smarty config
$set = array();
foreach ($smartyConfig as $key => $value) {
	$set[$key] = $value;
}


/*
$smarty->setTemplateDir(APPPATH . '/' . $application_folder . '/views/')->registerResource('file', new EvaledFileResource()); ;
$_ENV['smarty'] = $smarty;
*/

define('SMARTY_PATH', APPPATH . '/vendor/smarty/smarty/libs');

$app = new Application();
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\SwiftmailerServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app['swiftmailer.options'] = $mailConfig;
$app->register(new SmartyServiceProvider(), array(
    'smarty.class_path' => SMARTY_PATH,
    'smarty.options' => array(
        'template_dir'    => APPPATH . '/' . $application_folder . '/views/',
        'caching'         => false,
        'force_compile'   => false,
        'use_sub_dirs'    => false,
        //'default_modifiers' => array('escape:"htmlall"'),
    ),
));

$app['debug'] = true;
$app['smarty']->assign('setting', $set);
//Gọi các file core của hệ thống
$path = APPPATH . '/core';
$dh  = opendir($path);
while (false !== ($filename = readdir($dh))) {
	if(!in_array($filename,array(".",".."))) 
		require_once $path . '/' . $filename;
}

// Đặt bí danh cho các lớp
//class_alias('Cartalyst\Sentry\Facades\Native\Sentry', 'Sentry');
class_alias('Illuminate\Database\Capsule\Manager', 'DB');
class_alias('Silex\Application', 'Application');
class_alias('Symfony\Component\HttpFoundation\Request', 'Request');	

//Load Language file
$lang = Lang::getAll();

//Set login Attribute
User::setLoginAttributeName($config['login_attribute']);
User::setHasher(new $config['password_hasher']);
class Sentry extends \Cartalyst\Sentry\Facades\Native\Sentry
{
    public static function instance()
    {
    	global $config;
        return parent::instance(new UserProvider(new $config['password_hasher']));
    }
}

//Load Models
//controllers
$path = APPPATH . '/' . $application_folder . '/models';
$dh  = opendir($path);
while (false !== ($filename = readdir($dh))) {
	if(!in_array($filename,array(".",".."))) 
		require_once $path . '/' . $filename;
}

//Load Applications
//
//controllers
$path = APPPATH . '/' . $application_folder . '/controllers';
$dh  = opendir($path);
while (false !== ($filename = readdir($dh))) {
	if(!in_array($filename,array(".",".."))) 
		require_once $path . '/' . $filename;
}
//libraries
$path = APPPATH . '/' . $application_folder . '/libraries';
$dh  = opendir($path);
while (false !== ($filename = readdir($dh))) {
	if(!in_array($filename,array(".",".."))) 
		require_once $path . '/' . $filename;
}
//languages
$path = APPPATH . '/' . $application_folder . '/languages';
$dh  = opendir($path);
while (false !== ($filename = readdir($dh))) {
	if(!in_array($filename,array(".",".."))) 
		require_once $path . '/' . $filename;
}

?>