{include file="$view_dir/layouts/header.tpl"}
					<h3>Thông tin tài khoản</h3>
						<hr>
						<form action="" method="POST" class="form-horizontal" role="form">
							{include file="$view_dir/layouts/frontend-response.tpl"}
							<div class="form-group">
								<label for="inputName" class="col-sm-2 control-label">Tên đăng nhập:</label>
								<div class="col-sm-6">
									<input type="text" name="username" id="inputName" class="form-control" value="{$user->username}" disabled="disabled" required="required" title="">
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail" class="col-sm-2 control-label">Email:</label>
								<div class="col-sm-6">
									<input type="email" name="email" id="inputEmail" class="form-control" value="{$user->email}" required="required" title="">
								</div>
							</div>
							<div class="form-group">
								<label for="inputPhone" class="col-sm-2 control-label">Số điện thoại:</label>
								<div class="col-sm-6">
									<input type="text" name="phone" id="inputPhone" class="form-control" value="{$user->phone}" title="">
								</div>
							</div>
								<div class="form-group">
									<div class="col-sm-10 col-sm-offset-2">
										<button type="submit" class="btn btn-primary">Lưu</button>
									</div>
								</div>
						</form>	
{include file="$view_dir/layouts/footer.tpl"}