{include file="$view_dir/layouts/header.tpl"}

					{if isset($trans.code)}
						<h3>Chi tiết giao dịch #{$trans.code}</h3>
						<hr>
						<div class="col-sm-6">
						<a href="{$asset}history.html" class="btn btn-primary">Trở lại</a>
						<table class="table table-hover table-bordered">
							<tbody>
								<tr>
									<td>Mã giao dịch</td>
									<td>{$trans.code}</td>
								</tr>
								{foreach $trans.name as $k=>$v}
								<tr>
									<td>{$k}</td>
									<td>{$v}</td>
								</tr>
								{/foreach}
								<tr>
									<td>Số tiền</td>
									<td>{moneyFormat($trans.total)} USD</td>
								</tr>
								<tr>
									<td>Trạng thái</td>
									<td>
										{if $trans.status==1}
										<span class="label label-warning">Đang chờ</span>
										{else if $trans.status==2}
										<span class="label label-success">Hoàn thành</span>
										{else}
										<span class="label label-default">Đã huỷ</span>
										{/if}
									</td>
								</tr>
							</tbody>
						</table>
						
						</div>
					{else}
					<a href="{$asset}history.html" class="btn btn-primary">Trở lại</a>
					{/if}
						
{include file="$view_dir/layouts/footer.tpl"}