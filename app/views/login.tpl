{include file="$view_dir/layouts/header.tpl"}
					<h3>Đăng Nhập</h3>
						<hr>
						<form action="" method="POST" class="form-horizontal" role="form">
							{include file="$view_dir/layouts/frontend-response.tpl"}
							
							<div class="form-group">
								<label for="inputUsername" class="col-sm-2 control-label">Tên tài khoản:</label>
								<div class="col-sm-6">
									<input type="text" name="username" id="inputUsername" class="form-control" value="{$form.username}" required="required" title="">
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword" class="col-sm-2 control-label">Mật khẩu:</label>
								<div class="col-sm-6">
									<input type="password" name="password" id="inputPassword" class="form-control" required="required" title="">
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-6 col-sm-offset-2">
									<div class="checkbox">
										<label>
											<input type="checkbox" value="">
											Ghi nhớ đăng nhập
										</label>
									</div>
								</div>
							</div>
								<div class="form-group">
									<div class="col-sm-10 col-sm-offset-2">
										<button type="submit" class="btn btn-primary">Đăng Nhập</button>
									</div>
								</div>
						</form>
						<div class="col-sm-6 col-xs-offset-2">
							<p><a href="register.html">Đăng Ký</a> / <a href="forgot.html">Quên mật khẩu</a></p>
						</div>
{include file="$view_dir/layouts/footer.tpl"}