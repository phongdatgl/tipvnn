<?php 
use Symfony\Component\HttpFoundation\Response;
class HomeController {
	public function index (Application $app) 
	{
        $faqs = Pages::getPage(2);
		$app['smarty']->assign('title', 'Trang chủ');
		$app['smarty']->assign('faqs', $faqs);
		$topTipser = Tiplist::topTipser();
		$topMonthTipser = Tiplist::topTipser(true);
		$app['smarty']->assign('topTipser', $topTipser);
		$app['smarty']->assign('topMonthTipser', $topMonthTipser);
		return $app['smarty']->display('faqs.tpl');
	}
	public function homepage (Application $app) 
	{
		
		return 'Home page';
	}
	public function tips(Application $app, $page = 1)
	{
		global $set;
		$app['smarty']->assign('title', 'VIP TIP');
		$limit = 20;
		$offset = $page * $limit - $limit;
		$totalTips = Tiplist::totalTipVip();
		$totalPage = ceil($totalTips/$limit);
		$link = $set['site_url'] . 'tips-%s.html';
		$pagination = Pagination::init($totalPage, $page, $link, false);
		$app['smarty']->assign('pagination', $pagination);
		$tiplist = Tiplist::findTipVip($offset, $limit);
		$app['smarty']->assign('tiplist', $tiplist);

		$tipBought = array_unique(Transaction::getListTipBought());
		$app['smarty']->assign('tipBought', $tipBought);
		return $app['smarty']->display('tips.tpl');
	}
	public function freetips(Application $app, $page = 1)
	{
		global $set;
		$app['smarty']->assign('title', 'FREE TIP');
		$limit = 20;
		$offset = $page * $limit - $limit;
		$totalTips = Tiplist::totalTipFree();
		$totalPage = ceil($totalTips/$limit);
		$link = $set['site_url'] . 'free-tips-%s.html';
		$pagination = Pagination::init($totalPage, $page, $link, false);
		$app['smarty']->assign('pagination', $pagination);
		$tiplist = Tiplist::findTipFree($offset, $limit);
		$app['smarty']->assign('tiplist', $tiplist);
		return $app['smarty']->display('free-tips.tpl');
	}
	public function testtips(Application $app, $page = 1)
	{
		global $set;
		$app['smarty']->assign('title', 'TEST TIP');
		$limit = 20;
		$offset = $page * $limit - $limit;
		$totalTips = count(Tiplist::findTipTest());
		$totalPage = ceil($totalTips/$limit);
		$link = $set['site_url'] . 'free-tips-%s.html';
		$pagination = Pagination::init($totalPage, $page, $link, false);
		$app['smarty']->assign('pagination', $pagination);
		$tiplist = Tiplist::findTipTest($offset, $limit);
		$app['smarty']->assign('tiplist', $tiplist);
		return $app['smarty']->display('test-tips.tpl');
	}
	public function contact(Application $app)
	{
		$app['smarty']->assign('title', 'Liên hệ, góp ý');
		
		return $app['smarty']->display('contact.tpl');
	}
	public function doContact(Application $app)
	{
		global $set, $mailConfig;
		$mess = "Tên người liên hệ: " . $app['request']->get('name') . "<br>Email: " . $app['request']->get('email')
				. "<br>Tiêu đề: " . $app['request']->get('subject') . "<br>Nội Dung: " . $app['request']->get('message');
		$message = \Swift_Message::newInstance()
		        ->setSubject('['.$set["site_name"].'] Liên hệ mới')
		        ->setFrom(array($mailConfig['username']=>$set['site_name']))
		        ->setTo(array($set['admin_email']))
		        ->setBody($mess, 'text/html');

		if($app['mailer']->send($message))
			$app['session']->getFlashBag()->add('success', 'Cảm ơn bạn đã liên hệ với chúng tôi. Yêu cầu của bạn đã được tiếp nhận.');
		else 
			$app['session']->getFlashBag()->add('success', 'Hệ thống đang bị lỗi. Vui lòng liên hệ trực tiếp tới quản trị viên.');
		return $app->redirect('/contact.html', 301);
	}
	public function faqs(Application $app)
	{
		$faqs = Pages::getPage(2);
		$app['smarty']->assign('title', 'Hỏi đáp');
		$app['smarty']->assign('faqs', $faqs);
		$topTipser = Tiplist::topTipser();
		$topMonthTipser = Tiplist::topTipser(true);
		$app['smarty']->assign('topTipser', $topTipser);
		$app['smarty']->assign('topMonthTipser', $topMonthTipser);
		return $app['smarty']->display('faqs.tpl');
	}
	public function transactionHistory(Application $app, $page = 1)
	{
		global $set;
		$app['smarty']->assign('title', 'Lịch sử giao dịch');
		$user = Sentry::getUser();
		$limit = 10;
		$offset = $page * $limit - $limit;
		$app['smarty']->assign('stt', $offset + 1);
		$totalResult = count(Transaction::getTransactionList($user->id));
		$totalPage = ceil($totalResult/$limit);
		$link = $set['site_url'] . 'history-%s.html';
		$pagination = Pagination::init($totalPage, $page, $link, false);
		$app['smarty']->assign('pagination', $pagination);

		$transactions = Transaction::getTransactionList($user->id, $limit, $offset);	
		$app['smarty']->assign('transactions', $transactions);
		return $app['smarty']->display('history.tpl');
	}
	public function viewTransaction(Application $app, $id = 0)
	{
		$user = Sentry::getUser();
		$trans = Transaction::getTransactionById($user->id, $id);
		if( ! count($trans)) {
			$app['smarty']->assign('notfound', 'Không tìm thấy giao dịch');
		}
		$app['smarty']->assign('title', "Chi tiết giao dịch #" . @$trans['code']);
		$app['smarty']->assign('trans', $trans);
		return $app['smarty']->display('view-transaction.tpl');
	}
	public function buyTip(Application $app, $id = 0)
	{
		$tip = TipList::getTipById($id);
		if( ! count($tip)) {
			return new Response('Không tìm thấy TIP', 404 /* ignored */, array('X-Status-Code' => 200, 'Content-Type'=>'text/html'));
		}
		$tipBought = array_unique(Transaction::getListTipBought());
		if(in_array($id, $tipBought)) $app['smarty']->assign('bought', 1);
		else $app['smarty']->assign('bought', 0);
		
		$app['smarty']->assign('title', 'Xác nhận mua tip '.$tip["home"].' vs '.$tip["away"]);
		$app['smarty']->assign('tip', $tip);
		return $app['smarty']->display('buy-tip.tpl');
	}
	public function viewTip(Application $app, $id = 0)
	{
		$tip = TipList::getTipById($id);
		if( ! count($tip)) {
			return new Response('Không tìm thấy TIP', 404 /* ignored */, array('X-Status-Code' => 200, 'Content-Type'=>'text/html'));
		}
		$tipBought = array_unique(Transaction::getListTipBought());
		if( ! in_array($id, $tipBought)) { //chưa mua tip
			if(strtotime($tip['start_time'])>(time()-7200)) { //chưa diễn ra thì 404
				return new Response('Không tìm thấy TIP', 404 /* ignored */, array('X-Status-Code' => 200, 'Content-Type'=>'text/html'));
			}
		}
		
		$app['smarty']->assign('title', 'Chi tiết tip '.$tip["home"].' vs '.$tip["away"]);
		$app['smarty']->assign('tip', $tip);
		return $app['smarty']->display('view-tip.tpl');
	}
	public function buyConfirm(Application $app, $id = 0)
	{
		$tip = TipList::getTipById($id);
		if( ! count($tip)) {
			return new Response('Không tìm thấy TIP', 404 /* ignored */, array('X-Status-Code' => 200, 'Content-Type'=>'text/html'));
		}
		if(strtotime($tip['start_time'])+7200 > time()) {
			$user = Sentry::getUser();
			if($user->balance < $tip['price']) {
				$app['session']->getFlashBag()->add('error', 'Số tiền trong tài khoản của bạn không đủ để mua tip. <br>Bạn cần thêm '.moneyFormat($tip["price"]-$user->balance).' USD. <br><a href="/deposit.html">Bấm vào đây</a> để nạp tiền !');
			} else {
				$user->balance = $user->balance - $tip['price'];
				if($user->save()) {
					$data = array(
						'code'	=>	transaction_code_generator(),
						'create_at'	=>	date('Y-m-d H:i:s'),
						'total'	=>	$tip['price'],
						'user_id'	=>	$user->id,
						'buy_tip_id'	=>	$id,
						'type'	=>	2,
						'status'	=>	2,
						'meta'	=>	array(
							'meta_type'	=>	'Mua TIP',
							'tip_date'	=>	$tip['start_time'],
							'tip_league'=>	$tip['league'],
							'tip_vs'	=>	$tip['home'] . " vs " . $tip['away'],
							'tip_odds'	=>	$tip['odds'],
							'tip_tips'	=>	$tip['tips']
							//'tip_price'	=>	moneyFormat($tip['price']) . " VND"
							)
						);
					$ck = Transaction::insertNewTransaction($data);
					if($ck) {
						$app['session']->getFlashBag()->add('success', 'Mua TIP thành công !');
					} else {
						$app['session']->getFlashBag()->add('error', 'Mua TIP bị lỗi. Vui lòng liên hệ tới quản trị viên !');
					}
				} else {
					$app['session']->getFlashBag()->add('error', 'Có lỗi khi cập nhật !');
				}
			}
		} else {
			$app['session']->getFlashBag()->add('error', 'Trận đấu này đã diễn ra rồi, bạn không thể mua !');
		}
		//var_dump($app['session']->getFlashBag());
		return $app->redirect('/buy-tip-'.$id.'.html', 301);
	}
	public function ajax(Application $app)
	{
		$action = $app['request']->get('action');
		$data = array();
		if($action == 'tipserInfo') {
			$uid = $app['request']->get('uid');
			$userTip = Tiplist::tipByUser($uid);
			return json_encode($userTip);	
		}
	}
	
}
?>