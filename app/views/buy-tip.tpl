{include file="$view_dir/layouts/header.tpl"}

					{if isset($tip)}
					<h3>Xác nhận mua TIP:</h3>
						{include file="$view_dir/layouts/frontend-response.tpl"}
						<table class="table table-hover table-bordered">
							<tbody>
								{if $bought==0}
								<tr>
									<td colspan="2"><a href="confirm-{$tip.id}.html" onclick="return confirm('Bạn đã chắc chắn mua tip này ?');" class="btn btn-success">Mua Ngay</a></td>
								</tr>
								{else}
								<tr>
									<td colspan="2" class="bg-primary">Bạn đã mua tip này rồi</td>
								</tr>
								{/if}
								<tr>
									<td class="col-sm-2">Giờ</td>
									<td>{$tip.start_time}</td>
								</tr>
								<tr>
									<td>Mùa Giải</td>
									<td>{$tip.league}</td>
								</tr>
								<tr>
									<td>Trận</td>
									<td>{$tip.home} vs {$tip.away}</td>
								</tr>
								
								<tr>
									<td>Giá</td>
									<td>{moneyFormat($tip.price)} USD</td>
								</tr>
								{if $bought==1}
								<tr>
									<td>Odds</td>
									<td>{$tip.odds}</td>
								</tr>
								<tr>
									<td>Tips</td>
									<td>{$tip.tips}</td>
								</tr>
								<tr>
									<td>Thông tin</td>
									<td>{$tip.info}</td>
								</tr>
								{/if}
								{if $bought==0}
								<tr>
									<td colspan="2"><a href="confirm-{$tip.id}.html" onclick="return confirm('Bạn đã chắc chắn mua tip này ?');" class="btn btn-success">Mua Ngay</a></td>
								</tr>
								{/if}

							</tbody>
						</table>
					{else}
					<a href="{$asset}tips.html" class="btn btn-primary">Trở lại</a>
					{/if}
						
{include file="$view_dir/layouts/footer.tpl"}