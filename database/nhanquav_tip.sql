-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 12, 2014 at 09:40 AM
-- Server version: 5.5.36-cll-lve
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nhanquav_tip`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `groups_name_unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '{"admin":1,"uptipvip":1,"uptipfree":1,"user":1}', '2014-11-30 18:04:35', '2014-11-30 18:04:35'),
(2, 'VIP Tipser', '{"uptipvip":1,"uptipfree":1}', '2014-11-30 18:04:59', '2014-11-30 18:04:59'),
(3, 'Free Tipser', '{"uptipfree":1}', '2014-11-30 18:05:11', '2014-11-30 18:05:11'),
(4, 'User', '', '2014-12-02 16:54:51', '2014-12-02 16:54:51');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_content` text COLLATE utf8_unicode_ci NOT NULL,
  `page_type` int(11) NOT NULL,
  `sortorder` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `page_key`, `page_name`, `page_content`, `page_type`, `sortorder`) VALUES
(1, 'home_page', 'Home Page', '1. Tip Free là gì?<br>\r\nTipvnn có 1 đội ngũ tipster chuyên gia soi kèo nhận định bóng đá hằng ngày và liên tục 24/7. Để cung cấp Tip miễn phí \r\ndành cho các thành viên chưa có đủ điều kiện để đăng ký mua Tip.<br>\r\n2. Tại sao có 3 loại Tip 3 sao, 4 sao và 5 sao?<br>\r\n - Tip 3 sao là 70% Win, \r\n - Tip 4 sao là 80% Win,\r\n - Tip 5 sao là 95% Win (Đặc biệt với Tip 5 sao chỉ dành cho những kèo bán độ mà các ông trùm trong đường dây này tiết lộ ra)<br>\r\n3. Tại sao Tipvnn bán tip?<br>\r\nTipvnn đã có kinh nghiệm trong việc mua và sử dụng tips ở trong nước và cả ngoài nước. Và tất cả tip 3-5 Sao đều được chúng tôi<br>\r\nđăng ký mua ở các website Tip nước ngoài uy tin và chất lượng.<br>\r\nVì vậy chúng tôi cần có một nguồn thu vào để đăng ký Tip 5 sao với tỷ lệ win chắc chắn dành cho các thành viên của Tipvnn<br>\r\nVà cũng là một phần đầu tư cho server, website.<br>\r\n4. Tip ở Tipvnn có win 100% hay không?\r\nChúng tôi xin nhắc lại là Tip không thể win 100%. Tùy thuộc vào nhiều yếu tố như về phong độ của tipster nước ngoài\r\nĐặc biệt chúng tôi không khuyến khích các bạn mua và chia sẻ cho nhiều người biết Tip 5 Sao. Vì khi 1 người mua tip thì<br>\r\nchia sẻ cho rất nhiều người, chính vì điều đó sẽ dễ bị lật kèo khi nhà cái phát hiện.<br>\r\n5. Tipvnn khác với các website tip khác ở VN như thế nào?<br>\r\n- Tipvnn tự sản xuất tip là Free hoàn toàn<br>\r\n- Tất cả loại tip bán ra đều do chúng tôi đăng ký mua ở các website tip nước ngoài uy tín và chất lượng. <br>\r\n- Chúng tôi có nhiệm vụ đánh tips cùng các bạn. Đó mới là nguồn thu lớn nhất để chúng tôi duy trì mua tip mỗi ngày\r\n- Nếu tip thua thì chúng tôi có nhiệm vụ hoàn trả lại 100% số tiền cho thành viên mua tip ở Tipvnn<br>\r\n\r\nChú ý: Chúng tôi không khuyến khích các bạn chia sẻ cho nhiều người biết để cùng bet tip với chúng tôi. Chính điều đó có thể<br>\r\ngiúp cho nhà cái phát hiện sự chênh lệch bất thường làm ảnh hưởng đến % Tip Win.\r\n', 1, 0),
(2, '', '1. Tip Free là gì?', 'Tipsbetwin có 1 đội ngũ tipster chuyên gia soi kèo nhận định bóng đá hằng ngày và liên tục 24/7. Để cung cấp Tip miễn phí \r\ndành cho các thành viên chưa có đủ điều kiện để đăng ký mua Tip.', 2, 0),
(3, '', '2. Tại sao có 3 loại Tip 3 sao, 4 sao và 5 sao?', '- Tip 3 sao là 70% Win, (Tip 3 sao dành cho các tipster pro&nbsp;tự&nbsp;up khi có tỷ lệ win &gt;70% trong danh mục&nbsp;&nbsp;Tips Free)<br>- Tip 4 sao là 85% Win,\r\n<br>- Tip 5 sao là 99% Win (Đặc biệt với Tip 5 sao chỉ dành cho những kèo bán độ mà các ông trùm trong đường dây này tiết lộ ra)', 2, 0),
(4, '', '3. Tại sao TipsBetWin bán tip?', 'TipsBetWin đã có kinh nghiệm trong việc mua và sử dụng tips ở trong nước và cả ngoài nước. Và tất cả tip 4-5 Sao đều được chúng tôi đăng ký mua ở các website Tip nước ngoài uy tin và chất lượng.&nbsp;<br>Riêng Tip 3 sao được bán nhằm trả 1 phần &nbsp;lương cho các Tipster làm việc lâu năm gắn bó và soi kèo hằng ngày với chúng tôi. Cũng như 1 phần đóng góp vào duy trì và phát triển Website.', 2, 0),
(5, '', '4. Tip ở TipsBetWin có win 100% hay không?', '<div>Chúng tôi xin nhắc lại là Tip không thể win 100%. Tùy thuộc vào nhiều yếu tố như về phong độ của tipster nước ngoài.</div><div>Nhưng các bạn yên tâm, TipsBetWin&nbsp; đủ khả năng giúp các bạn chiến thắng lâu dài và ổn định. Nếu Tip thua thì TipsBetWin cũng thất bại lớn về tiền bạc mua tip và sự uy tín.&nbsp;Vì vậy sẽ hoàn trả 100% khi khách hàng mua Tips thua.</div>', 2, 0),
(6, '', '5. TipsBetWin khác với các website tip khác ở VN như thế nào?', '<div>- TipsBetWin tự sản xuất tip là Free hoàn toàn</div><div>- Tất cả loại tip bán ra đều do chúng tôi đăng ký mua ở các website tip nước ngoài uy tín và chất lượng.&nbsp;</div><div>- Chúng tôi có nhiệm vụ đánh tips cùng các bạn. Đó mới là nguồn thu lớn nhất để chúng tôi duy trì mua tip mỗi ngày</div><div>- Nếu tip thua thì chúng tôi có nhiệm vụ hoàn trả lại 100% số tiền cho thành viên mua tip ở TipsBetWin</div>', 2, 0),
(7, '', '6. Làm sao tin được record ở TipsBetWin là chính xác và không chỉnh sửa', '<div>Xin thưa các bạn là TipsBetWin&nbsp; đặt sự uy tin và chất lượng lên hàng đầu. Vì vậy các bạn yên tâm khi đặt mua 1 tip ở TipsBetWin &nbsp;&nbsp; thì các bạn<br></div><div>sẽ nhìn thấy thông tin chi tiết tips đó được post lúc nào (có thời gian cụ thể) bằng hình ảnh minh họa như demo bên dưới khi giao dịch thành công!</div>', 2, 0),
(8, '', '7. Cách hoạt động: ', '<div>Tất cả tipster VN tại TipsBetWin đều phải trải qua up kèo Tip Free cho mem. Và có nhiệm vụ góp vốn mua tip nước ngoài cùng đánh với thành viên&nbsp;TipsBetWin&nbsp;để có trách&nbsp;nhiệm và gây quỹ giúp cho Tip bán ra với chi phí rẻ nhất có thể.</div><div>Chỉ những tipster nào có tỷ lệ win trong tháng &gt;70% thì mới được lên làm mod up kèo Tip 3 sao dành cho thành viên với &nbsp; &nbsp;giá rẻ. Nếu up tip bán ra liên tiếp thua 3 3 kèo thì tipster đó phải quay lại trở về up Tip Free miễn phí cho mem tham khảo.</div><div>Chính vi vậy đội ngũ tipster có sự cạnh tranh và 100% đảm bảo chất lượng cùng sự uy tín cho TipsBetWin</div>', 2, 0),
(9, '', '8/ Hướng dẫn mua Tip tại Tipvnn', 'Click xem video hướng dẫn (đang update)', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `setting_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `setting_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `setting_key`, `setting_name`, `setting_value`) VALUES
(1, 'site_name', 'Site Name', 'TipsBetWin.COM'),
(2, 'site_url', 'Site URL', 'http://tipsbetwin.com/'),
(3, 'admin_email', 'Admin Email', 'phongdatgl@gmail.com'),
(4, 'facebook_url', 'Facebook Fanpage', 'https://facebook.com/tipvnn.com'),
(5, 'twitter_url', 'Twitter Page', 'https://twitter.com/username'),
(6, 'gplus_url', 'Google Plus', 'https://plus.google.com/000000'),
(7, 'top_banner', 'Top Banner(540x80)', 'http://tipsbetwin.com/assets/img/560x80.png'),
(8, 'top_banner_link', 'Link Top', 'http://google.com'),
(9, 'right_banner', 'Right Banner(245x200)', 'http://tipsbetwin.com/assets/img/245x200.jpg'),
(10, 'right_banner_link', 'Link Right', 'http://google.com'),
(11, 'logo_url', 'Logo URL', ''),
(12, 'aff_percent', 'Tipser', '20'),
(13, 'refund_percent', 'Hoàn Trả', '70');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE IF NOT EXISTS `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(4) NOT NULL DEFAULT '0',
  `banned` tinyint(4) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Dumping data for table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `ip_address`, `attempts`, `suspended`, `banned`, `last_attempt_at`, `suspended_at`, `banned_at`) VALUES
(1, 1, '0.0.0.0', 1, 0, 0, '2014-12-02 17:07:29', NULL, NULL),
(2, 3, '0.0.0.0', 0, 0, 0, NULL, NULL, NULL),
(3, 2, '0.0.0.0', 0, 0, 0, NULL, NULL, NULL),
(4, 4, '0.0.0.0', 0, 0, 0, NULL, NULL, NULL),
(5, 7, '0.0.0.0', 0, 0, 0, NULL, NULL, NULL),
(6, 8, '171.252.72.238', 0, 0, 0, NULL, NULL, NULL),
(7, 1, '171.252.72.238', 0, 0, 0, NULL, NULL, NULL),
(8, 1, '14.167.37.171', 0, 0, 0, NULL, NULL, NULL),
(9, 9, '14.167.37.171', 0, 0, 0, NULL, NULL, NULL),
(10, 10, '14.167.37.171', 0, 0, 0, NULL, NULL, NULL),
(11, 11, '14.167.37.171', 0, 0, 0, NULL, NULL, NULL),
(12, 12, '27.78.212.5', 0, 0, 0, NULL, NULL, NULL),
(13, 8, '27.73.112.59', 0, 0, 0, NULL, NULL, NULL),
(14, 1, '27.73.112.59', 0, 0, 0, NULL, NULL, NULL),
(15, 4, '27.73.112.59', 0, 0, 0, NULL, NULL, NULL),
(16, 13, '27.73.112.59', 0, 0, 0, NULL, NULL, NULL),
(17, 14, '14.165.177.36', 0, 0, 0, NULL, NULL, NULL),
(18, 11, '27.66.169.137', 0, 0, 0, NULL, NULL, NULL),
(19, 15, '113.162.241.89', 0, 0, 0, NULL, NULL, NULL),
(20, 12, '27.78.98.77', 0, 0, 0, NULL, NULL, NULL),
(21, 13, '171.255.130.236', 0, 0, 0, NULL, NULL, NULL),
(22, 1, '116.100.166.17', 0, 0, 0, NULL, NULL, NULL),
(23, 15, '103.237.65.9', 0, 0, 0, NULL, NULL, NULL),
(24, 13, '113.160.224.67', 0, 0, 0, NULL, NULL, NULL),
(25, 13, '27.73.192.234', 0, 0, 0, NULL, NULL, NULL),
(26, 15, '115.77.160.216', 0, 0, 0, NULL, NULL, NULL),
(27, 12, '27.79.88.175', 0, 0, 0, NULL, NULL, NULL),
(28, 1, '14.167.36.216', 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tiplist`
--

CREATE TABLE IF NOT EXISTS `tiplist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `league` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `home` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `odds` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `away` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tips` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `score` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `isvip` int(11) NOT NULL DEFAULT '0',
  `result` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `info` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_refund` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Dumping data for table `tiplist`
--

INSERT INTO `tiplist` (`id`, `start_time`, `league`, `home`, `odds`, `away`, `tips`, `score`, `type`, `isvip`, `result`, `price`, `info`, `user_id`, `is_refund`) VALUES
(15, '2014-12-07 20:00', 'RUSSIA U21 CHAMPIONSHIP', 'Spartak Moscow U21 Ural Sverdlovsk ', '-1.5@1.76', 'Oblast U21', 'Spartak Moscow U21 Ural Sverdlovsk ', '3-3', 3, 1, 2, 50, '', 1, 0),
(16, '2014-12-07 20:00', 'GERMANY REGIONAL LEAGUE NORTH', 'Werder Bremen AM', '- 0.5-1@186', 'St. Pauli AM', 'Werder Bremen AM', '1-1', 3, 1, 2, 50, '', 9, 0),
(17, '2014-12-07 20:30', 'BELGIUM JUPILER PRO LEAGUE', 'Club Brugge Zulte ', '-1@1.85', 'Waregem', 'Club Brugge Zulte ', '2-1', 4, 1, 3, 150, '', 10, 0),
(18, '2014-12-08 02:45', 'SERIE A', 'Inter Milan', 'Over 2.5@1.96', 'Udinese', 'Over', '1-2', 4, 1, 1, 150, '', 10, 0),
(19, '2014-12-08 03:00', 'France ', 'MARSEILLE', '1.85', 'METZ', 'MARSEILLE(-1)', '3-1', 5, 1, 1, 250, '', 12, 0),
(20, '2014-12-07 23:00', 'Potugal D2', 'FC PORTO B', '1.85', 'ATLETICO CLUBE DE PORTUGAL', 'FC PORTO B(-0.75)', '1-1', 3, 1, 2, 50, '', 12, 0),
(21, '2014-12-09 02:00', 'HA LAN D2   ', 'JONG AJAX AMSTERDAM', '1.88', 'RKC WAALWIJK', 'Home (-1)', '3+2', 5, 1, 3, 250, '', 12, 0),
(22, '2014-12-10 02:45', 'Cup C1', 'Real Madrid', '1.95', 'Ludogorets Razgrad', 'Home (-2.5)', '4+0', 4, 1, 1, 200, '', 12, 0),
(23, '2014-12-11 19:00', 'AFF CUP', 'VIETNAM', '1.99', 'MALAYSIA', 'Hone(-1)', '2+4', 3, 1, 2, 200, '', 12, 0),
(24, '2014-12-12 01:00', 'UEFA LG', 'BORUSSIA MONCHENGLADBACH', '1.95', 'FC ZURICH', 'Home(-1.25)', '3+0', 5, 1, 1, 300, '', 12, 0),
(25, '2014-12-12 03:05', ' UEFA Europa League', 'Legia Warszawa', '82', 'Trabzonspor', 'Legia Warszawa  -0.25', '2-0', 0, 0, 1, 0, '', 15, 0),
(26, '2014-12-13 02:30', 'FRANCE 1', 'LYON', '1.90', 'CAEN', 'Hone(-1)', '', 5, 1, 0, 300, '', 12, 0),
(27, '2014-12-13 02:45', 'SPAIN LALIGA', 'UD Almeria', '79', 'Real Madrid', 'Real Madrid  -2', '', 0, 0, 0, 0, '', 15, 0),
(28, '2014-12-13 02:30', 'FRANCE LIGUE 1', 'Olympique Lyonnais', '85', 'SM Caen', 'Olympique Lyonnais   -1', '', 0, 0, 0, 0, '', 15, 0);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `create_at` datetime NOT NULL,
  `total` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `buy_tip_id` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=31 ;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `code`, `create_at`, `total`, `user_id`, `buy_tip_id`, `type`, `status`) VALUES
(1, 'G11MTFKUKF8K47BO', '2014-12-02 00:19:22', 5, 1, 1, 1, 2),
(2, 'R2G6TWM7TZCQX6OB', '2014-12-02 00:19:24', 25, 1, 4, 1, 3),
(3, '33ECUSC5YFKJM5WH', '2014-12-02 00:24:09', 100, 1, 0, 1, 1),
(4, 'FGUCFAVKL87SKY7H', '2014-12-02 00:24:14', 100, 1, 0, 1, 1),
(5, 'F0N8T2ABEWW90H1Q', '2014-12-02 00:27:58', 100, 1, 0, 1, 1),
(6, '07Q4WH5OCXC5OHA5', '2014-12-02 20:40:27', 5, 1, 0, 1, 1),
(10, '1WYS8F951Q2V36UX', '2014-12-02 23:43:48', 5, 3, 9, 2, 2),
(11, 'KUUEJX8ZRBRLE793', '2014-12-04 21:55:25', 5, 1, 9, 2, 2),
(12, '7GI2IXG6JKN27WT0', '2014-12-04 22:20:12', 5, 3, 0, 1, 1),
(13, 'NFZXPXWXO0SMI8AA', '2014-12-04 22:20:51', 25, 3, 10, 2, 2),
(14, 'HTXFI69XPXANGI2Q', '2014-12-04 23:51:15', 5, 1, 0, 1, 1),
(16, 'ARSLYI0H5SLL0TGV', '2014-12-06 17:14:38', 3, 1, 0, 1, 2),
(17, 'FJLVKI66LWKLMXK2', '2014-12-06 19:39:43', 4, 3, 0, 1, 2),
(18, '9BI1MA8K7K508B1F', '2014-12-06 19:39:43', 4, 1, 0, 1, 2),
(19, 'GLFGHV88LUCI1EPT', '2014-12-06 19:41:10', 4, 3, 0, 1, 2),
(20, 'JUR7A0JE1AGCZHHQ', '2014-12-06 19:41:11', 4, 1, 0, 1, 2),
(21, 'Z36RKW9UY0HNNLZ0', '2014-12-06 20:36:41', 4, 3, 0, 1, 2),
(22, 'K6K96XC9IE6BZO6C', '2014-12-06 20:36:41', 4, 1, 0, 1, 2),
(23, 'Q7B10ILRW5R0IERR', '2014-12-06 23:42:55', 4, 3, 0, 1, 2),
(24, 'CWXHMX14U6J55W5L', '2014-12-06 23:42:55', 4, 1, 0, 1, 2),
(25, 'XWF4AE4WWXVM7MV1', '2014-12-06 23:43:39', 4, 3, 0, 1, 2),
(26, '80VKE820OV6QK0GK', '2014-12-06 23:43:39', 4, 1, 0, 1, 2),
(27, 'RGXWQOUZQF7KGYIP', '2014-12-06 23:44:06', 4, 3, 0, 1, 2),
(28, '47N0HH4KQLXZKXIB', '2014-12-06 23:44:06', 4, 1, 0, 1, 2),
(29, 'NCOT26E9APKNKO6G', '2014-12-06 23:45:34', 4, 3, 0, 1, 2),
(30, 'JHPUHZ3E2HT7KVVV', '2014-12-06 23:45:34', 4, 1, 0, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_meta`
--

CREATE TABLE IF NOT EXISTS `transaction_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_id` int(11) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=142 ;

--
-- Dumping data for table `transaction_meta`
--

INSERT INTO `transaction_meta` (`id`, `trans_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'account_number', '111'),
(2, 1, 'account_name', '222'),
(3, 1, 'bank_name', 'Đông Á'),
(4, 2, 'account_number', '111'),
(5, 2, 'account_name', '222'),
(6, 2, 'bank_name', 'Đông Á'),
(7, 3, 'account_number', '111'),
(8, 3, 'account_name', '222'),
(9, 3, 'bank_name', 'Đông Á'),
(10, 4, 'account_number', '111'),
(11, 4, 'account_name', '222'),
(12, 4, 'bank_name', 'Đông Á'),
(13, 5, 'account_number', '0103231111'),
(14, 5, 'account_name', 'dat'),
(15, 5, 'bank_name', 'Đông Á'),
(16, 5, 'deposit_time', '2014-12-01 00:27'),
(17, 6, 'account_number', '0103231888'),
(18, 6, 'account_name', 'Lê Hùng Quốc'),
(19, 6, 'bank_name', 'Đông Á'),
(20, 6, 'deposit_time', '2014-12-02 05:30'),
(21, 7, 'meta_type', 'Mua TIP'),
(36, 10, 'meta_type', 'Mua TIP'),
(37, 10, 'tip_date', '2014-12-03 10:00:00'),
(38, 10, 'tip_league', 'Cúp VLeague'),
(39, 10, 'tip_vs', 'Việt Nam vs Lào'),
(40, 10, 'tip_odds', '1.25'),
(41, 10, 'tip_tips', 'Under 2.5'),
(42, 11, 'meta_type', 'Mua TIP'),
(43, 11, 'tip_date', '2014-12-05 00:00'),
(44, 11, 'tip_league', 'Cúp VLeague'),
(45, 11, 'tip_vs', 'Cam Pu Chia vs Lào'),
(46, 11, 'tip_odds', '0.5'),
(47, 11, 'tip_tips', 'Under 2.5'),
(48, 12, 'account_number', '0103231111'),
(49, 12, 'account_name', 'Quốc'),
(50, 12, 'bank_name', 'Đông Á'),
(51, 12, 'deposit_time', '2014-12-04 22:20'),
(52, 13, 'meta_type', 'Mua TIP'),
(53, 13, 'tip_date', '2014-12-05 22:15'),
(54, 13, 'tip_league', 'UEFA-EURO U19 Championship'),
(55, 13, 'tip_vs', 'Anh vs Pháp'),
(56, 13, 'tip_odds', '0.5'),
(57, 13, 'tip_tips', 'Over 2.5'),
(58, 14, 'account_number', '0103231111'),
(59, 14, 'account_name', '123123'),
(60, 14, 'bank_name', 'Đông Á'),
(61, 14, 'deposit_time', '2014-12-04 23:50'),
(62, 15, 'aff_type', 'Trả tiền hoa hồng user mua tip'),
(63, 15, 'aff_total', '11'),
(64, 15, 'aff_tran', 'Trận Cam Pu Chia vs Lào'),
(65, 15, 'aff_percent', '20'),
(66, 15, 'aff_time', '2014-12-06 17:14:10'),
(67, 16, 'aff_type', 'Trả tiền hoa hồng cho Tipser'),
(68, 16, 'aff_total', '11$'),
(69, 16, 'aff_tran', 'Trận Cam Pu Chia vs Lào'),
(70, 16, 'aff_percent', '20%'),
(71, 16, 'aff_time', '2014-12-06 17:14:38'),
(72, 17, 'ref_type', 'Hoàn trả tiền mua TIP (thua)'),
(73, 17, 'ref_bought', '5$'),
(74, 17, 'ref_receive', 'Nhận lại 4$'),
(75, 17, 'ref_percent', '70%'),
(76, 17, 'ref_time', '2014-12-06 19:39:43'),
(77, 18, 'ref_type', 'Hoàn trả tiền mua TIP (thua)'),
(78, 18, 'ref_bought', '5$'),
(79, 18, 'ref_receive', 'Nhận lại 4$'),
(80, 18, 'ref_percent', '70%'),
(81, 18, 'ref_time', '2014-12-06 19:39:43'),
(82, 19, 'ref_type', 'Hoàn trả tiền mua TIP (thua)'),
(83, 19, 'ref_bought', '5$'),
(84, 19, 'ref_receive', 'Nhận lại 4$'),
(85, 19, 'ref_percent', '70%'),
(86, 19, 'ref_time', '2014-12-06 19:41:10'),
(87, 20, 'ref_type', 'Hoàn trả tiền mua TIP (thua)'),
(88, 20, 'ref_bought', '5$'),
(89, 20, 'ref_receive', 'Nhận lại 4$'),
(90, 20, 'ref_percent', '70%'),
(91, 20, 'ref_time', '2014-12-06 19:41:11'),
(92, 21, 'ref_type', 'Hoàn trả tiền mua TIP (thua)'),
(93, 21, 'ref_bought', '5$'),
(94, 21, 'ref_receive', 'Nhận lại 4$'),
(95, 21, 'ref_percent', '70%'),
(96, 21, 'ref_time', '2014-12-06 20:36:41'),
(97, 22, 'ref_type', 'Hoàn trả tiền mua TIP (thua)'),
(98, 22, 'ref_bought', '5$'),
(99, 22, 'ref_receive', 'Nhận lại 4$'),
(100, 22, 'ref_percent', '70%'),
(101, 22, 'ref_time', '2014-12-06 20:36:41'),
(102, 23, 'ref_type', 'Hoàn trả tiền mua TIP (thua)'),
(103, 23, 'ref_bought', '5$'),
(104, 23, 'ref_receive', 'Nhận lại 4$'),
(105, 23, 'ref_percent', '70%'),
(106, 23, 'ref_time', '2014-12-06 23:42:55'),
(107, 24, 'ref_type', 'Hoàn trả tiền mua TIP (thua)'),
(108, 24, 'ref_bought', '5$'),
(109, 24, 'ref_receive', 'Nhận lại 4$'),
(110, 24, 'ref_percent', '70%'),
(111, 24, 'ref_time', '2014-12-06 23:42:55'),
(112, 25, 'ref_type', 'Hoàn trả tiền mua TIP (thua)'),
(113, 25, 'ref_bought', '5$'),
(114, 25, 'ref_receive', 'Nhận lại 4$'),
(115, 25, 'ref_percent', '70%'),
(116, 25, 'ref_time', '2014-12-06 23:43:39'),
(117, 26, 'ref_type', 'Hoàn trả tiền mua TIP (thua)'),
(118, 26, 'ref_bought', '5$'),
(119, 26, 'ref_receive', 'Nhận lại 4$'),
(120, 26, 'ref_percent', '70%'),
(121, 26, 'ref_time', '2014-12-06 23:43:39'),
(122, 27, 'ref_type', 'Hoàn trả tiền mua TIP (thua)'),
(123, 27, 'ref_bought', '5$'),
(124, 27, 'ref_receive', 'Nhận lại 4$'),
(125, 27, 'ref_percent', '70%'),
(126, 27, 'ref_time', '2014-12-06 23:44:06'),
(127, 28, 'ref_type', 'Hoàn trả tiền mua TIP (thua)'),
(128, 28, 'ref_bought', '5$'),
(129, 28, 'ref_receive', 'Nhận lại 4$'),
(130, 28, 'ref_percent', '70%'),
(131, 28, 'ref_time', '2014-12-06 23:44:06'),
(132, 29, 'ref_type', 'Hoàn trả tiền mua TIP (thua)'),
(133, 29, 'ref_bought', '5$'),
(134, 29, 'ref_receive', 'Nhận lại 4$'),
(135, 29, 'ref_percent', '70%'),
(136, 29, 'ref_time', '2014-12-06 23:45:34'),
(137, 30, 'ref_type', 'Hoàn trả tiền mua TIP (thua)'),
(138, 30, 'ref_bought', '5$'),
(139, 30, 'ref_receive', 'Nhận lại 4$'),
(140, 30, 'ref_percent', '70%'),
(141, 30, 'ref_time', '2014-12-06 23:45:34');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `activated` tinyint(4) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `balance` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_activation_code_index` (`activation_code`),
  KEY `users_reset_password_code_index` (`reset_password_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `permissions`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `first_name`, `last_name`, `phone`, `balance`, `created_at`, `updated_at`) VALUES
(1, 'phongdatgl@gmail.com', 'admin', '8JJTgbczMyqE0ej4b64c315eeaee10bad74408d47eebc28623d31091bdb701e88a06c89aa66c5a60', NULL, 1, NULL, NULL, '2014-12-12 20:20:05', 'UKwOQi3TXUnQxNSI1fef1d5673ec7dc1ca06f204df9ca9ad4e746cff4b30fb1a1190be0b943c8ebe', NULL, NULL, NULL, '0123123123', 900047, '2014-11-30 18:05:54', '2014-12-13 01:20:05'),
(2, 'viptip@gmail.com', 'viptip', 'FenRnEOqhL6v8CWLd5645e29bcdf3cde39dcdad8b57ee38969d5f697aa61a85350ab8914c88fb86c', NULL, 1, NULL, NULL, '2014-12-04 22:15:51', 'jgCWwKr5FdSrvW6w4e67b42eb1928c689956d210988d97c6a1a7f98944f9668f7a945ec8773c8122', NULL, NULL, NULL, '123123', 0, '2014-12-02 17:08:23', '2014-12-04 15:15:51'),
(3, 'freetip@tipvnn.com', 'freetip', 'mcY3nF76ggl1n8pwc2a6b29603d64ffd3ccc49de99e88f024b78799a26f0bb904926c9b0ed0cfca9', NULL, 1, NULL, NULL, '2014-12-06 20:22:37', 'rc5TuqdKzQ2Ob2Ege5e3e881973e4d2d7340d5c4d6b19cae400372d64593fb168997c8473661a164', NULL, NULL, NULL, '', 5, '2014-12-03 13:04:06', '2014-12-07 13:53:26'),
(4, 'user@tipvnn.com', 'user', 'IhBaRq7X4z1dNGAoec04f7836bfa07d2e4fdcd3a58386fed84e8e74ef6cc87b342fc9c4c99520d1b', NULL, 1, NULL, NULL, '2014-12-07 15:59:47', 'bkEAr1qpGI5pXEwP3e7346af4c64698d30c57a9a37a08af23c7bbaf1c39c1c1218b05937a936401a', NULL, NULL, NULL, '', 0, '2014-12-03 13:04:37', '2014-12-07 20:59:47'),
(6, 'nguoidung1@gmail.com', 'nguoidung1', 'ShtxyIz1O84QYe3F92bf60b2d527cdd57e900ce613a611f0821cf4a6d8e2966eb10990c3bf69332f', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '123123', 10, '2014-12-04 13:37:14', '2014-12-07 13:53:41'),
(7, 'nguoidung2@gmail.com', 'nguoidung2', 'PyER9o2vSSHrvpGQa78e6718fe35f29417e3ec62bd5f91ff364c848412dc78f44c06b1e03a23fa9f', NULL, 0, NULL, NULL, '2014-12-04 20:38:25', 'wmu8bi9JdTVxHIiTceb154849bfa4e1453834545b1ef2cca1cec2d9629e01e20000ae89749427089', NULL, NULL, NULL, '0123.123.123', 0, '2014-12-04 13:38:13', '2014-12-04 14:09:21'),
(8, 'user01@gmail.com', 'user01', '9vevvFRMfLHUhVnQ5369b315e8492f4739a26fbd3fb91fc4ddb27389234edc142137ae86fd4c59dd', NULL, 1, NULL, NULL, '2014-12-07 15:58:01', 'OwCTcCraMORuB7ZQ8e149849e8e497c49c38f84605564363c95de341de4f0f025f442cfc5ebaccc6', NULL, NULL, NULL, '', 0, '2014-12-07 13:51:51', '2014-12-07 20:58:01'),
(9, 'tipsterken@yahoo.com.vn', 'tipsterken', '2cWHo0MAVNSmtnVI06564e84680d6946510383e9476d6e0f38449c14c0f631bd88549ce220414464', NULL, 1, NULL, NULL, '2014-12-07 13:01:52', 'vANcJpP25e6wxMqa739634f00aff743dda95152b0dc9f99aaaff552bfb96b318533e938fb63e0de8', NULL, NULL, NULL, '123', 0, '2014-12-07 18:01:34', '2014-12-07 18:01:52'),
(10, '123@yahoo.com', 'tipsterking', 'hlfbpl2Jr8bfmSLE5fd7193477be13a698244a1417ef8417adf965f567d16a670c9899be8c224ec7', NULL, 1, NULL, NULL, '2014-12-07 13:38:03', 'QrlMSoIaZGuxbSmW677492d4f82dac1288c1f9af09946004d40d7a8ececc36b1d724365d1c8e01d8', NULL, NULL, NULL, '123', 0, '2014-12-07 18:37:22', '2014-12-07 18:38:03'),
(11, 'lhq214@yahoo.com', 'lhq214', 'orUfdSgRIPsbviNe633deb94513fcd0240253024a7c75c118ffb097b69b79ba01c99c79a89ee1337', NULL, 1, NULL, NULL, '2014-12-09 05:54:42', 'KvJXpkbDNFl0m782433612759da40adee3351683abc6cfcb10b5dde87d9d7613b97fbf015af58c8f', NULL, NULL, NULL, '0906596259', 0, '2014-12-07 18:53:23', '2014-12-09 10:54:42'),
(12, 'phamquang1069@gmail.com', 'davidhon', 'c1zPldDhCXWo9myg17e7130bceeeabde86d2392e70d3cf33490c2e4031bbd576fc1c51f5fa8d1e99', NULL, 1, NULL, NULL, '2014-12-12 12:31:54', 'h0lGWRrtSj0mvC6bd3a01902e56b869381312bf04d0e3c4f8ea403d1783496b2ebb1e269f8eb688d', NULL, NULL, NULL, '84919795069', 0, '2014-12-07 19:43:17', '2014-12-12 17:31:54'),
(13, 'developer@gmail.com', 'developer', 'tUzqXvw93zh6BMHpa42fb5cd8d6142baa38cbd92fc82cae7ef93a19172232a4dfe442158fde9157f', NULL, 1, NULL, NULL, '2014-12-11 19:14:10', 'ThIEB9kfSz8svG1uba5b56aa0a5bca29b4e27719660922d9566a72f74acdfaccf59ee00707c3f61d', NULL, NULL, NULL, '', 0, '2014-12-07 21:04:21', '2014-12-12 00:14:10'),
(14, 'betwingroupvietnam@gmail.com', 'vietnambet', 'Pt2U6kB4MYwDCs86535de8a21eeacc8fc703c625df4ce4d55dc7d392cb23611360b22f21c0e80f6e', NULL, 1, NULL, NULL, '2014-12-08 01:52:20', 'o9zsXKa9H8qlqP0o0b8d0ff27151eb2d210f90871a0db4664a7454d1313977a9c8f677f4fc0fa31a', NULL, NULL, NULL, '0949633983', 0, '2014-12-08 06:51:00', '2014-12-08 06:52:20'),
(15, 'hoainhon1209@gmail.com', 'singo', 'lhBi8icPv7RVJauge6c41625e87f8a92b2455ed5f6d3af511b696da6557615c941fdaf62d06aa894', NULL, 1, NULL, NULL, '2014-12-12 08:35:25', '8S4PuXPUuEJ1mw7b3c1470b8988f44e4404c75463461566b0e1738a3364334d59ace9aceaa0d6618', NULL, NULL, NULL, '0907969390', 0, '2014-12-09 19:44:34', '2014-12-12 13:35:25');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(9, 6, 4),
(11, 7, 3),
(12, 8, 4),
(14, 9, 2),
(16, 10, 2),
(17, 11, 4),
(19, 12, 2),
(20, 13, 1),
(22, 14, 2),
(24, 15, 3);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
