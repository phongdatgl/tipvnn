<?php 
class BankingController {
	public function bankDeposit(Application $app)
	{
		$user = Sentry::getUser();
		$app['smarty']->assign('user', $user);
		$app['smarty']->assign('title', 'Nạp tiền qua ngân hàng');
		$app['smarty']->assign('form', $app['session']->getFlashBag()->get('formValue'));
		//$app['smarty']->assign('error', $app['session']->getFlashBag()->get('error'));
		//$app['smarty']->assign('success', $app['session']->getFlashBag()->get('success'));
		return $app['smarty']->display('bank-deposit.tpl');
	}
	public function doBankDeposit(Application $app)
	{
		$user = Sentry::getUser();
		$app['session']->getFlashBag()->set('formValue', array(
						'account_number'=>$app['request']->get('account_number'),
						'account_name'=>$app['request']->get('account_name'),
						'bank_name'=>$app['request']->get('bank_name')
						));
		$data = array(
			'code'		=>	transaction_code_generator(),
			'create_at'	=>	date('Y-m-d H:i:s'),
			'total'		=>	$app['request']->get('total'),
			'user_id'	=>	$user->id,
			'type'		=>	1,
			'status'	=>	1,
			'meta'		=>	array(
							'account_number'	=>	$app['request']->get('account_number'),
							'account_name'		=>	$app['request']->get('account_name'),
							'bank_name'			=>	$app['request']->get('bank_name'),
							'deposit_time'		=>	$app['request']->get('deposit_time')
							)
			);
		if($trans_id = Transaction::insertNewTransaction($data)) {
			$app['session']->getFlashBag()->set('success', 'Yêu cầu của bạn đã được gửi đi.');
			$app['session']->getFlashBag()->set('formValue', array());
		} else {
			$app['session']->getFlashBag()->set('error', 'Có lỗi khi gửi yêu cầu.');
		}
		return $app->redirect('/deposit.html', 301);
	}
	public function baokimDeposit(Application $app)
	{
		return $app['smarty']->display('baokim.tpl');
	}
}
