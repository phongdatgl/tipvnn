<?php 
define ('APPPATH', dirname(__FILE__)); 
define ('INCODE', TRUE);
define ('HOST', 'http://tipvnn.dev/');
//include config file
include_once "loader.php";
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

$setting = DB::table('settings')->get();
$set = array();
foreach ($setting as $key => $value) {
	$set[$value['setting_key']] =  $value['setting_value'];
}
$app['smarty']->assign('setting', $set);
$app['smarty']->assign('view_dir', APPPATH . '/' . $application_folder . '/views');
$app['smarty']->assign('asset', $set['site_url']);
$uri = substr($_SERVER['REQUEST_URI'], 1, strlen($_SERVER['REQUEST_URI']));
$app['smarty']->assign('uri', $uri);
if(stristr($uri, 'admin/')) {
	$waiting = DB::table('transactions')->where('status','=', 1)->count();
	$app['smarty']->assign('totalWaiting', $waiting);
}

include_once "router.php";
$app->run();
?>