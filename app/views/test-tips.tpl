{include file="$view_dir/layouts/header.tpl"}
<h4>Các TIPS trong Test Tipser là các tips của tipser đăng ký tham gia chương trình <a href="/tuyentipster.html" target="_blank">Tuyển TIPSTER siêu dự bị tại TipsBetWin</a></h4>
					<div class="table-responsive">
							<table class="table table-hover table-tripped">
								<thead>
									<tr>
										<th>YY-MM-DD</th>
										<th>LEAGUE</th>
										<th>HOME</th>
										<th>ODDs</th>
										<th>AWAY</th>
										<th>TIP</th>
										<th>Score</th>
										<th>User UP</th>
										<th>TL WIN</th>
										<th>RESULT</th>
									</tr>
								</thead>
								<tbody>
									{foreach $tiplist as $v}
									<tr>
										<td>{if strtotime($v.start_time)>(time()-7200)}
											{date('Y-m-d H:i', strtotime($v.start_time))}
											{else}
											{date('Y-m-d', strtotime($v.start_time))}
											{/if}
										</td>
										<td>{$v.league}</td>
										<td>{$v.home}</td>
										<td>{$v.odds}</td>
										<td>{$v.away}</td>
										<td>{$v.tips}</td>
										<td>{if $v.score!=''}{$v.score}{else}?{/if}</td>
										<td>{if $v.percent<20}<span class="text-danger" title="New Tipser">{$v.username}</span>{else}{$v.username}{/if}</td>
										<td id="p_{$v.user_id}">{moneyFormat($v.percent)} %</td>
										<td>
											{if $v.result==0}
											<span class="label label-default"><i class="fa fa-question"></i></span>
											{else if $v.result==1}
											<span class="label label-success tip-win">WIN</span>
											{else if $v.result==2}
											<span class="label label-danger">LOSE</span>
											{else}
											<span class="label label-info">DRAW</span>
											{/if}
										</td>
									</tr>
									{/foreach}
								</tbody>
							</table>
						</div>
						<div id="pagination" class="pull-center">
							{$pagination}
						</div>

						<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tipser Info</h4>
      </div>
      <div class="modal-body">
        <form action="" method="POST" class="form-horizontal" role="form">
        	<div class="form-group">
        		<label for="input" class="col-sm-2 control-label">Username:</label>
        		<div class="col-sm-10" style="padding-top: 10px;" id="musername">---</div>
        	</div>
        	<div class="form-group">
        		<label for="input" class="col-sm-2 control-label">Total Tips:</label>
        		<div class="col-sm-10" style="padding-top: 10px;" id="mtotaltip">---</div>
        	</div>
        	<div class="form-group">
        		<label for="input" class="col-sm-2 control-label">TL Win:</label>
        		<div class="col-sm-10" style="padding-top: 10px;" id="mwinpercent">---</div>
        	</div>
        	<div class="form-group">
        		<label for="input" class="col-sm-2 control-label">Rank:</label>
        		<div class="col-sm-10" style="padding-top: 10px;" id="mrank">---</div>
        	</div>
        	<div class="form-group">
        		<label for="input" class="col-sm-2 control-label">Tip WIN:</label>
        		<div class="col-sm-10" style="padding-top: 10px;" id="mwin">---</div>
        	</div>
        	<div class="form-group">
        		<label for="input" class="col-sm-2 control-label">Tip LOSE:</label>
        		<div class="col-sm-10" style="padding-top: 10px;" id="mlose">---</div>
        	</div>
        	<div class="form-group">
        		<label for="input" class="col-sm-2 control-label">Tip DRAW:</label>
        		<div class="col-sm-10" style="padding-top: 10px;" id="mdraw">---</div>
        	</div>
        	
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
{include file="$view_dir/layouts/footer.tpl"}