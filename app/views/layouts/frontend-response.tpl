							{$error = $app.session->getFlashBag()->get('error')}
							{if isset($error)}{if $error}
							<div class="form-group">
								<div class="col-sm-12">
									<div class="alert alert-danger">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										{$error.0}
									</div>
								</div>
							</div>
							{/if}{/if}
							{$success = $app.session->getFlashBag()->get('success')}
							{if isset($success)}{if $success}
							<div class="form-group">
								<div class="col-sm-12">
									<div class="alert alert-success">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										{$success.0}
									</div>
								</div>
							</div>
							{/if}{/if}