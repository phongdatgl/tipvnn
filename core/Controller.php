<?php 
namespace Core;
class Controller {
	public static function load($name, $params = array()) {
		global $application_folder;
		if(stristr($name, '@')) {
			$line = explode('@', $name);
			$method = $line[1];
			$class_name = $line[0];
		} else {
			$class_name = $name;
			$method = 'index';
		}
		if(is_array($params)) {
			$datas = $params;
		} else {
			$datas = array($params);
		}
		call_user_func(array(new $class_name, $method));
		//$controller = new $class_name;
		//$controller->$method($params);
		return '';
	}
}
?>