{include file="$view_dir/layouts/header.tpl"}
						<h3>Nạp tiền vào tài khoản</h3>
						<hr>
						<p>Xin chào <strong>{$user->username}</strong>, Bạn hiện có <strong style="color: #FF0000;">{moneyFormat($user->balance)}</strong> USD</p>
						<div role="tabpanel">
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><a href="deposit.html">Chuyển Khoản</a></li>
								<li role="presentation"><a href="baokim.html">Bảo Kim</a></li>
							</ul>
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="bank">
									<i>Chuyển tiền qua tài khoản ngân hàng. Số tiền sẽ cập nhật tối đa là 24h kể từ khi bạn yêu cầu</i>
									<div class="panel panel-danger">
									    <div class="panel-heading" role="tab" id="headingOne">
									      	<h4 class="panel-title">
									        	<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									          	HƯỚNG DẪN NẠP TIỀN
									        	</a>
									      	</h4>
									    </div>
									    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									      	<div class="panel-body">
									        <ul>
									        	<li>B1: Nạp (hoặc chuyển) tiền vào một trong hai tài khoản dưới đây:
													<ul>
														<li><span class="text-danger"><strong>Đông Á Bank</strong></span>
															<ul>
																<li>Số tài khoản: <strong>010 323 1888</strong></li>
																<li>Tên tài khoản: <strong>Lê Hùng Quốc</strong></li>
																
															</ul>
														</li>
														<li><span class="text-danger"><strong>Vietcombank</strong></span>
															<ul>
																<li>Số tài khoản: <strong>0751 00000 4205</strong></li>
																<li>Tên tài khoản: <strong>Lê Hùng Quốc</strong></li>
																
															</ul>
														</li>
													</ul>
									        	</li>
									        	<li>B2: Nhập thông tin vào khung bên dưới và bấm <span class="text-danger"><strong>GỬI ĐI</strong></span>
									        		<ul>
									        			<li>Đối với hình thức nạp tiền trực tiếp tại quầy. Điền số tài khoản và tên tài khoản là <span class="text-danger"><strong>NAPTIEN</strong></span></li>
									        			<li>Đối với hình thức chuyển tiền, vui lòng ghi đúng số tài khoản và tên tài khoản đã chuyển</li>
									        		</ul>
									        	</li>
									        	<li>B3: Nếu trong 6h tiền vẫn chưa vào tài khoản của bạn, vui lòng nhắn tin tới số điện thoại: <span class="text-danger"><strong>0188.241.8670</strong></span> với nội dung: 
													<ul>
														<li><span class="text-danger"><strong>TIP TENTAIKHOAN SOTIEN</strong></span></li>
														<li>Chúng tôi sẽ kiểm tra và cập nhật tiền vào tài khoản cho bạn (chậm nhất là 1 giờ kể từ khi yêu cầu ).</li>

													</ul>
									        	</li>

									        </ul>
									      	</div>
									    </div>
									</div>
									<form action="" method="POST" class="form-horizontal" role="form">
										<div class="form-group">
											<p class="col-sm-12">Nhập thông tin bạn đã chuyển khoản vào khung bên dưới</p>
										</div>
										{include file="$view_dir/layouts/frontend-response.tpl"}
										<div class="form-group">
											<label for="inputStk" class="col-sm-2 control-label">Số tài khoản:</label>
											<div class="col-sm-6">
												<input type="text" name="account_number" id="inputStk" class="form-control" value="{if isset($form.0.account_number)}{$form.0.account_number}{/if}" required="required" title="">
											</div>
										</div>
										<div class="form-group">
											<label for="inputTentk" class="col-sm-2 control-label">Tên tài khoản:</label>
											<div class="col-sm-6">
												<input type="text" name="account_name" id="inputTentk" class="form-control" value="{if isset($form.0.account_name)}{$form.0.account_name}{/if}" required="required" title="">
											</div>
										</div>
										<div class="form-group">
											<label for="inputBankname" class="col-sm-2 control-label">Ngân hàng:</label>
											<div class="col-sm-6">
												<select name="bank_name" id="inputBankname" class="form-control" required="required">
													<option value="Đông Á">Đông Á</option>
													<option value="Vietcombank">Vietcombank</option>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label for="inputTotal" class="col-sm-2 control-label">Số tiền nạp:</label>
											<div class="col-sm-2">
												<div class="input-group">
												  	<input type="text" name="total" id="inputTotal" class="form-control" value="{if isset($form.0.total)}{$form.0.total}{/if}" required="required" pattern="(\d+)" title="">
												  	<span class="input-group-addon">$</span>
												</div>
												
											</div>
											<div class="col-sm-8"><i>Tỉ giá: 1$ = 20.000 VND</i></div>
										</div>
										<div class="form-group">
											<label for="inputTime_dep" class="col-sm-2 control-label">Thời gian nạp:</label>
											<div class="col-sm-4">
												<div class='input-group datetimepicker'>
							                    	<input type="text" name="deposit_time" id="inputTime_dep" readonly="true" data-date-format="YYYY-MM-DD HH:mm" class="form-control" value="{if isset($form.0.total)}{$form.0.total}{else}{date('Y-m-d H:i')}{/if}" required="required" title="">
							                    	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							                    </div>
											</div>
										</div>
											<div class="form-group">
												<div class="col-sm-10 col-sm-offset-2">
													<button type="submit" class="btn btn-primary">Gửi đi</button>
												</div>
											</div>
									</form>
								</div>
    							<div role="tabpanel" class="tab-pane" id="cards">
    								
    							</div>
							</div>
						</div>
{include file="$view_dir/layouts/footer.tpl"}