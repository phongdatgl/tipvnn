{include file="$view_dir/layouts/header.tpl"}
					<h3>Đăng ký tài khoản</h3>
						<hr>
						<form action="" method="POST" class="form-horizontal" role="form">
							{include file="$view_dir/layouts/frontend-response.tpl"}
							<div class="form-group">
								<label for="inputUsername" class="col-sm-2 control-label">Tên đăng nhập:</label>
								<div class="col-sm-6">
									<input type="text" name="username" id="inputUsername" class="form-control" value="{$form.username}" required="required" title="">
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword" class="col-sm-2 control-label">Mật khẩu:</label>
								<div class="col-sm-6">
									<input type="password" name="password" id="inputPassword" class="form-control" required="required" title="">
								</div>
							</div>
							<div class="form-group">
								<label for="inputConfirm" class="col-sm-2 control-label">Nhập lại:</label>
								<div class="col-sm-6">
									<input type="password" name="confirm" id="inputConfirm" class="form-control" required="required" title="">
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail" class="col-sm-2 control-label">Email:</label>
								<div class="col-sm-6">
									<input type="email" name="email" id="inputEmail" class="form-control" required="required" value="{$form.email}" title="">
								</div>
							</div>
							<div class="form-group">
								<label for="inputPhone" class="col-sm-2 control-label">Phone:</label>
								<div class="col-sm-6">
									<input type="text" name="phone" id="inputPhone" class="form-control" value="{$form.phone}" title="">
								</div>
							</div>
							<div class="form-group">
								<label for="inputCapt" class="col-sm-2 control-label">Captcha:</label>
								<div class="col-sm-4">
									<div class="input-group captcha">
										<input type="text" name="captcha" class="form-control">
										<span class="input-group-addon"><img src="{$asset}captcha.html?time={time()}"></span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Đăng ký làm:</label>
								<div class="col-sm-10">
									<div class="radio">
										<label>
											<input type="radio" name="user_type" id="" value="1" checked="checked">
											User
										</label><br>
										<label>
											<input type="radio" name="user_type" id="" value="4">
											Test Tipser
										</label><br>
										<label>
											<input type="radio" name="user_type" id="" value="2">
											Free Tipser
										</label><br>
										<label>
											<input type="radio" name="user_type" id="" value="3">
											VIP Tipser
										</label>

									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-10 col-sm-offset-2">
									<button type="submit" class="btn btn-primary">Đăng Ký</button>
								</div>
							</div>
	
						</form>
						<div class="col-sm-6 col-xs-offset-2">
							<p><a href="login.html">Đăng nhập</a> / <a href="forgot.html">Quên mật khẩu</a></p>
						</div>
{include file="$view_dir/layouts/footer.tpl"}