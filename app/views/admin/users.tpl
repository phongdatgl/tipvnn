{include file="$view_dir/layouts/header_admin.tpl"}
{include file="$view_dir/layouts/frontend-response.tpl"}
<a href="{$admin_dir}/add-user.html" class="btn btn-success">Thêm User</a>
<form action="" method="GET" class="form-inline pull-right" role="form">
	<div class="form-group">
		<div class="col-sm-10">
			<input type="text" name="keyword" id="inputKeyword" placeholder="Email, Username, ..." class="form-control" value="{if isset($keyword)}{$keyword}{/if}" title="">
		</div>
	</div>
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
<table class="table table-hover table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Username</th>
			<th>Email</th>
			<th>Total</th>
			<th>Ngày đăng ký</th>
			<th>Group</th>
			<th>Trạng thái</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		{foreach $users as $v}
		<tr>
			<td>{$stt}</td>
			<td>{$v.username}</td>
			<td>{$v.email}</td>
			<td>{$v.balance} $</td>
			<td>{$v.ucreated_at}</td>
			<td><span class="label label-{if $v.group_id==1}danger{else if $v.group_id==2}primary{else if $v.group_id==3}info{else}warning{/if}">{$v.gname}</span></td>
			<td>{if $v.activated==1}
				<span class="label label-success">Activated</span>
				{else}
				<span class="label label-default">inActivated</span>
				{/if}
			</td>
			
			<td><a href="{$admin_dir}/edit-user-{$v.uid}.html" title="Edit" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a> <a href="{$admin_dir}/delete-user-{$v.uid}.html" onclick="return confirm('Bạn có muốn xoá không ?');" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-close"></i></a> </td>
		</tr>
		{$stt = $stt + 1}
		{/foreach}
	</tbody>
</table>
<div id="pagination" class="pull-center">
	{$pagination}
</div>

{include file="$view_dir/layouts/footer.tpl"}