{include file="$view_dir/layouts/header.tpl"}
					<div class="table-responsive">
							<table class="table table-hover table-tripped">
								<thead>
									<tr>
										<th>TIME</th>
										<th>LEAGUE</th>
										<th>HOME</th>
										<th>AWAY</th>
										<th>TIP</th>
										<th>SCORE</th>
										<th class="tip-type">TYPE</th>
										<th>RESULT</th>
										<th>INFO</th>
									</tr>
								</thead>
								<tbody>
									{foreach $tiplist as $v}
									<tr>
										<td>{if strtotime($v.start_time)>(time()-7200)}
											{date('Y-m-d H:i', strtotime($v.start_time))}
											{else}
											{date('Y-m-d', strtotime($v.start_time))}
											{/if}
										</td>
										<td>{$v.league}</td>
										<td>{$v.home}</td>
										<td>{$v.away}</td>
										<td>
											{if strtotime($v.start_time)>(time()-7200) && $v.tips!=''}
												{if in_array($v.id, $tipBought)}
													<span class="tip-bought">{$v.tips}</span>
												{else}
													<a href="buy-tip-{$v.id}.html" data-toggle="tooltip" data-placement="top" title="Mua tip này với giá: {moneyFormat($v.price)} USD" class="btn btn-sm btn-success">Mua</a>
												{/if}
											{else}
												{$v.tips}
											{/if}
										</td>
										<td>{if $v.score!=''}{$v.score}{else}?{/if}</td>
										<td class="tip-type t{$v.type}-star">
											{for $i=1 to $v.type}<i class="fa fa-star"></i>{/for}
										</td>
										<td>
											{if $v.result==0}
											<span class="label label-default"><i class="fa fa-question"></i></span>
											{else if $v.result==1}
											<span class="label label-success tip-win">WIN</span>
											{else if $v.result==2}
											<span class="label label-danger">LOSE</span>
											{else}
											<span class="label label-info">DRAW</span>
											{/if}
										</td>
										<td>{if strtotime($v.start_time)>(time()-7200) && $v.tips!=''}
												{if in_array($v.id, $tipBought)}
													<a href="viewtip-{$v.id}.html" class="btn btn-sm btn-success">VIEW</a>
												{/if}
											{else}
												<a href="viewtip-{$v.id}.html" class="btn btn-sm btn-success">VIEW</a>
											{/if}
										</td>
									</tr>
									{/foreach}
								</tbody>
							</table>
						</div>
						<div id="pagination" class="pull-center">
							{$pagination}
						</div>
{include file="$view_dir/layouts/footer.tpl"}