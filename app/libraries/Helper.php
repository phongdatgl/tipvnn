<?php 
if( ! function_exists('moneyFormat'))
{
	function moneyFormat($money)
	{
		return number_format($money, 0, ',', '.');
	}
}
if( ! function_exists('transaction_code_generator'))
{
	function transaction_code_generator($length = 16)
	{
		$str = 'ABCEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		do {
			$code = '';
			for ($i=1; $i <= $length; $i++) { 
				$code .= $str[rand(0, strlen($str)-1)];
			}
			$ck = DB::table('transactions')->where('code', '=', $code)->first();
		} while(count($ck) != 0);
		
		return $code;
	}
}
?>