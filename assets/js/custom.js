jQuery(document).ready(function() {
	$('[data-toggle="tooltip"]').tooltip();
	$('.datetimepicker').datetimepicker({
        language: 'vi',
        //useSeconds: true, 
        showToday: true,
        pick12HourFormat: true
    });
	
});
$('.captcha .input-group-addon img').click(function(event) {
	$(this).attr('src', asset + 'captcha.html?time='+$.now());
});
$('#inputType').change(function() {
	var val = $(this).val();
	if(parseInt(val) >= 3) {
		$('#isvip').removeClass('vip-hidden');
	} else {
		$('#isvip').addClass('vip-hidden');
	}
});
/*
$('.wysihtml5').wysihtml5({
    	"style": true,
	    "font-styles": false,
	    "emphasis": false,
	    "lists": false,
	    "html": true,
	    "link": false,
	    "image": true,
	    "color": false,
	    parser: function(html) {
	        return html;
	    }

    });
*/
//nicEditors.allTextAreas();

$('#save-transaction').click(function(event) {
	$('#save-transaction').addClass('disabled');
	var status = $('#new-status').val();
	$.ajax({
		url: asset + 'admin/save-transaction.html',
		data: {id: $('#trans-id').val(), status: status},
		type: 'POST',
		success: function(res) {
			if(status==1)
				html = '<span class="label label-warning">Waiting</span>';
			else if (status==2)
				html = '<span class="label label-success">Completed</span>';
			else html = '<span class="label label-danger">Cancelled</span>';
			$('#trans-result').html(html);
			$('#save-transaction').removeClass('disabled');
		},
		error: function () {
			alert('Ajax Error !');
			$('#save-transaction').removeClass('disabled');
		}
	})
});
if ( $( "#nicEditor" ).length ) {
	area2 = new nicEditor({fullPanel : true}).panelInstance('nicEditor');
}
/*
(function() {var _h1= document.getElementsByTagName('title')[0] || false;
var product_name = ''; if(_h1){product_name= _h1.textContent || _h1.innerText;}var ga = document.createElement('script'); ga.type = 'text/javascript';
ga.src = '//live.vnpgroup.net/js/web_client_box.php?hash=1defd5288377523b572ff893ceb5ea88&data=eyJzc29faWQiOjIwMjc4LCJoYXNoIjoiMGUzZjkzNGJlN2Q1ZDYyOWFiZTY1ZGU1NGIxYTVhZDQifQ--&pname='+product_name;
var s = document.getElementsByTagName('script');s[0].parentNode.insertBefore(ga, s[0]);})();
*/
//chat zopim
/*
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2genWZodHo63Y0AwklWYVv65f3wWLmlj';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
*/

$('a.show-tipser').click(function(event) {
	uid = this.id.split('_');
	uid = uid[1];
	$.ajax({
		url: asset + 'ajax.html',
		data: {action: 'tipserInfo', uid: uid},
		type: 'POST',
		dataType: 'json',
		success: function(json) {
			console.log(json);
			$('#musername').text(json.username);
			$('#mtotaltip').text(json.totaltip);
			$('#mrank').text(json.position + '/' + json.total);
			$('#mwin').text(json.win);
			percent = $('#p_'+uid).html();
			//percent = percent + "%";
			$('#mwinpercent').text(percent);
			$('#mlose').text(json.lose);
			$('#mdraw').text(json.draw);
		}
	})
});