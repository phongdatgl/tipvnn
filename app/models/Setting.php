<?php 
class Setting extends Model {
	protected static $table = 'settings';
	public static function getSettings()
	{
		return DB::table(self::$table)->get();
	}
	public static function saveSettings($data)
	{
		foreach ($data as $key => $value) {
			DB::table(self::$table)->where('setting_key','=',$key)->update(array('setting_value'=>$value));
		}
		return true;
	}
}