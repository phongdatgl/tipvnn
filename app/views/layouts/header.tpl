<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>{if isset($title)}{$title}{else}Trang Chủ{/if} - {$setting.site_name}</title>

		<!-- Bootstrap CSS -->
		<link href="{$asset}assets/css/bootstrap.min.css" rel="stylesheet">
		<!-- Font-Awesome -->
		<link href="{$asset}assets/css/font-awesome.min.css" rel="stylesheet">
		<!-- Font -->
		<link href="{$asset}assets/css/fonts.css" rel="stylesheet">
		<!-- Plugins -->
		<link href="{$asset}assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
		<!-- WISIHTML5 -->
		<!--<link rel="stylesheet" type="text/css" href="{$asset}assets/css/bootstrap-wysihtml5.css">-->
		<!-- Styles -->
		<link href="{$asset}assets/css/style.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script type="text/javascript">
		var asset = '{$asset}';
		</script>
	</head>
	<body>
	<div id="wrap">
		<div id="header">
			<div class="container">
				<div class="row">
					<div class="col-xs-12" id="main-header">
						<div class="col-xs-6">
							<h1 class="site_name"><a href="/index.html">{if $setting.logo_url==''}{$setting.site_name}{else}<img src="{$setting.logo_url}" alt="{$setting.site_name}">{/if}</a></h1>
							<!--<p class="site-description">Chuyên gia nhận định</p>-->
						</div>
						<div class="col-xs-6">
							<div id="top-ads">
								<a href="{$setting.top_banner_link}" target="_blank"><img src="{$setting.top_banner}" class="img-responsive"></a>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<div id="nav">
			<div class="container">
				<div class="row">
					<nav class="navbar navbar-default" role="navigation">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						{$check = Sentry::check()}
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse navbar-ex1-collapse">
							<ul class="nav navbar-nav">
								{assign var=navbar value=[
								['url'=>'index.html', 'name'=>'Trang Chủ'],
								['url'=>'tips.html', 'name'=>'VIP TIPS'],
								['url'=>'free-tips.html', 'name'=>'FREE TIPS'],
								['url'=>'test-tips.html', 'name'=>'Test Tipser'],
								['url'=>'contact.html', 'name'=>'Liên Hệ'],
								['url'=>'faqs.html', 'name'=>'Hỏi Đáp'],
								['url'=>'tuyentipster.html', 'name'=>'TUYỂN TIPSER']
								]}
								{foreach $navbar as $k=>$v}
								<li class="{if $uri==$v.url}active{/if}"><a href="/{$v.url}">{$v.name}</a></li>
								{/foreach}
								{if $check}
								<li class="{if $uri=='deposit.html'}active{/if}"><a href="/deposit.html">Nạp tiền</a></li>
								{/if}
							</ul>
							
							<ul class="nav navbar-nav navbar-right">
								<li>
									<ul class="ul-inline socials">
								   		<li><a href="{$setting.facebook_url}" target="_blank"><span class="fa fa-facebook"></span></a></li>
								   		<li><a href="{$setting.twitter_url}" target="_blank"><span class="fa fa-twitter"></span></a></li>
								   		<li><a href="{$setting.gplus_url}" target="_blank"><span class="fa fa-google-plus"></span></a></li>
								   </ul>
								</li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">Tài Khoản <b class="caret"></b></a>
									<ul class="dropdown-menu">
										{assign var=dropdown value=[
											['url'=>'register.html', 'name'=>'Đăng ký', 'class'=>'fa-plus', 'type'=>1],
											['url'=>'login.html', 'name'=>'Đăng nhập', 'class'=>'fa-user', 'type'=>1],
											['url'=>'forgot.html', 'name'=>'Quên mật khẩu', 'class'=>'fa-question', 'type'=>1],
											['url'=>'index.html', 'name'=>'QUẢN TRỊ', 'class'=>'fa-cogs', 'type'=>3],
											['url'=>'profile.html', 'name'=>'Thông tin tài khoản', 'class'=>'fa-gear', 'type'=>2],
											['url'=>'change-pass.html', 'name'=>'Đổi mật khẩu', 'class'=>'fa-lock', 'type'=>2],
											['url'=>'deposit.html', 'name'=>'Nạp tiền', 'class'=>'fa-bank', 'type'=>2],
											['url'=>'history.html', 'name'=>'Lịch sử giao dịch', 'class'=>'fa-history', 'type'=>2],
											['url'=>'logout.html', 'name'=>'Thoát', 'class'=>'fa-sign-out', 'type'=>2]
											]}
								   		{foreach $dropdown as $k=>$v}
											{if $check}
												{$user = Sentry::getUser()}
												
												{if $v.type==3 && $user->hasAccess('uptipfree')}
													<li><a href="{$admin_dir}/{$v.url}">{$v.name}</a></li>
												{else if $v.type==3 && $user->hasAccess('testtip')}
													<li><a href="{$admin_dir}/{$v.url}">{$v.name}</a></li>
												{/if}
												{if $v.type==2}
													<li><a href="{$asset}{$v.url}">{$v.name}</a></li>
												{/if}
											{else}
												{if $v.type==1}
													<li><a href="{$asset}{$v.url}">{$v.name}</a></li>
												{/if}
											{/if}
										{/foreach}
									</ul>
								</li>
								
							</ul>
						</div><!-- /.navbar-collapse -->
					</nav>
				</div>
			</div>
		</div> <!-- /#nav -->
		<div id="primary">
			<div class="container">
				<div class="row" id="main-primary">
					<div class="col-sm-12" id="main-content">