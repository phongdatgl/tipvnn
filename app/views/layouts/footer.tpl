</div>
					
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div id="footer">
			<div class="container">
				<div class="row">
					<div class="col-xs-12" id="main-footer">
						<p class="pull-center copyright">
							Copyright 2014 {$setting.site_name}
						</p>
						<p class="pull-center copyright">
							All rights reserved
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
		<!-- jQuery -->
		<script src="{$asset}assets/js/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="{$asset}assets/js/bootstrap.min.js"></script>
		<!-- Plugins -->
		<script src="{$asset}assets/js/moment.js"></script>
		<script src="{$asset}assets/js/bootstrap-datetimepicker.js"></script>
		<!--<script src="{$asset}assets/js/wysihtml5-0.3.0.js"></script>
		<script src="{$asset}assets/js/bootstrap-wysihtml5.js"></script>-->
		<script src="{$asset}assets/js/nicEdit.js"></script>

		<!-- My Script -->
		<script src="{$asset}assets/js/custom.js"></script>
		
	</body>
</html>