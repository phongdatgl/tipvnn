{include file="$view_dir/layouts/header.tpl"}
<div style="margin-top: 20px;"></div>
	<ul class="nav nav-pills">
		{assign var=admin_menu value=[
			['url'=>'/index.html', 'name'=>'HOME', type=>1],
			['url'=>'/tiplist.html', 'name'=>'TIPS', type=>1],
			['url'=>'/users.html', 'name'=>'USERS', type=>2],
			['url'=>'/settings.html', 'name'=>'SETTINGS', type=>2],
			['url'=>'/transactions.html', 'name'=>'TRANSACTIONS', type=>2],
			['url'=>'/pages.html', 'name'=>'PAGES', type=>2]
			]} 
		{$user = Sentry::getUser()}
		{foreach $admin_menu as $v}
		{$current = substr("{$admin_dir}{$v.url}", 1, strlen("{$admin_dir}{$v.url}"))}
			{if $v.type==2}
				{if stristr($v.url,'/transactions.html')}{$v.name = "TRANSACTIONS <span class='badge'>$totalWaiting</span>"}{/if}
				{if stristr($v.url,'/transactions.html')}{if $totalWaiting > 0}{$v.url='/transactions.html?status=1'}{/if}{/if}
				{if $user->hasAccess('admin')}<li role="presentation" class="{if stristr($uri, $current)}active{/if}"><a href="{$admin_dir}{$v.url}">{$v.name}</a></li>{/if}
			{else}
			<li role="presentation" class="{if $uri==$current}active{/if}"><a href="{$admin_dir}{$v.url}">{$v.name}</a></li>
			{/if}

		{/foreach}
	  	
	</ul>
	<hr>
<div style="margin-top: 20px;"></div>